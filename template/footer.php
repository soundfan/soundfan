	<!--modal for login starts-->
	<!-- Modal -->
    
	  	<div class="modal fade" id="login_model" tabindex="-1" data-backdrop="false" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      		<div class=" modal-backdrop" style="opacity:0.5;"></div>
			<div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                	<h4 class="modal-title">Login</h4>
                </div>
                <div class="modal-body" style="word-wrap: break-word;">
                    <center>
                        We ask you to login so that we will be able to show link songs to your account and show you statistics. We will not post anything on your behalf. We promise.
                        <br /> <br />                  
                        <div class="row">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-3">
                            	<div id="btn-login">
			                    	<fb:login-button max_rows="1" size="large" show_faces="false" auto_logout_link="false" scope="public_profile,email" onlogin="facebookLogin();">
				                    </fb:login-button>
				                </div>
                    		</div>
                            <div class="col-lg-2"></div>
                            <div class="col-lg-3">                        
                            	<a href="login-twitter.php"><img src="images/twitter.png"></a>
                            </div>
                        </div>
                    </center>             
                </div>
                
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
	  </div>
    </div><!-- /.modal -->
	<!-- modal for login ends -->


      <footer>
        
      </footer>
    


    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    
    <!-- auto post switch -->
    <script src="js/bootstrap-switch.js"></script>
    
	<script src="js/bootswatch.js"></script>
	
	<!-- File Upload -->
	<script src="js/bootstrap-fileupload.min.js"></script>
	<script src="js/bootstrap-fileupload.js"></script>
    
    
    <!-- PHONE FORMATTER -->
	<script src="js/bootstrap-formhelpers-phone.format.js"></script>    
	<script src="js/bootstrap-formhelpers-phone.js"></script>
    
    <?php
		if($load_audio == "no"){
	?>
    <!-- Audio Player -->
	<script src="js/audioplayer.min.js"></script>
    <script>
		$(function() { 
			$( 'audio' ).audioPlayer({
				classPrefix: 'audioplayer',
				strPlay: 'Play',
				strPause: 'Pause',
				strVolume: 'Volume'
			});
	 	});
  	</script>
    <script>
		(function(doc){var addEvent='addEventListener',type='gesturestart',qsa='querySelectorAll',scales=[1,1],meta=qsa in doc?doc[qsa]('meta[name=viewport]'):[];function fix(){meta.content='width=device-width,minimum-scale='+scales[0]+',maximum-scale='+scales[1];doc.removeEventListener(type,fix,true);}if((meta=meta[meta.length-1])&&addEvent in doc){fix();scales=[.25,1.6];doc[addEvent](type,fix,true);}}(document));
	</script>
	<?php }?>
 
 	<script src="js/general.js"></script>
    
    <script>
		
		$(document).ready(function(){
			var modal_flag = true;
			//$(".audioplayer-playpause").click(function(){
			$("#music_player").bind("play",function(){
				$("#music_player").bind("ended", function(){$(this).clearQueue(); var next_url = $("#next_url").val();  window.location=next_url; });
				
				if(modal_flag)
				{
					setTimeout(function()  {  $('#feedbackmodel').modal('show');  }, 5000);
					modal_flag = false;
					
					var songid = $("#songid").val();
					$.post("songlisten_count.php",{song_id: songid,listen:"1"});			// for count and auto post...
					
					$("#feedbackmodel").ready(function(){
						$( "#love_song" ).bind( "click", function() {
							$("#lovehatestatus").val("1");
							$("#love_song, #hate_song " ).hide();
							$('#loveratemodel').modal('show');
							var song = $(this).val();
							$.post("love_hate_song.php",{song_id:song,hate:"0",love:"1"},function(data){$("#update_count").html(data)});
						});
						
						$( "#hate_song" ).bind( "click", function() {
							$("#lovehatestatus").val("0");							
							$("#love_song, #hate_song " ).hide();
							$('#loveratemodel').modal('show');
							var song = $(this).val();
							$.post("love_hate_song.php",{song_id:song,hate:"1",love:"0"},function(data){$("#update_count").html(data)});
						});
						
						/*	Backup
						$( "#hate_song" ).bind( "click", function() {
							$("#love_song, #hate_song " ).hide();
							var song = $(this).val();
							var next_url = $("#next_url").val();
							$.post("love_hate_song.php",{song_id:song,hate:"1",love:"0"},function(){window.location=next_url;});
						});
						*/
						
						$( "#skip_song" ).bind( "click", function() {
							var next_url = $("#next_url").val();
							window.location=next_url; 
						});
													console.log(1);
						$( "#dont_play" ).bind( "click", function() {
							console.log(2);
							$.post("dont_play_appln.php",{song_id:songid},function(data){
								console.log(3);
								var next_url = $("#next_url").val();
								window.location=next_url; 
							});
						});
					});
				}

			});
			
			
			// comment submit after rating
			$("#loveratemodel").ready(function(){
				$("#submitcomment").click(function(){
					
					if($('#inputUserComment').val() != ''){
						comment = $('#inputUserComment').val();
						song_id	= $('#song_id').val();
						var next_url = $("#next_url").val();
						var status = $("#lovehatestatus").val();
						$.post("insert_comment.php",{comment:comment, song_id:song_id, status: status},	function(data){ 
																							if(status == "1") 
																								$('#inputUserComment_div').html(data);
																							else{
																								$('#inputUserComment_div').html(data);
																								window.location=next_url; 
																							}
																						});
					
					}
					else{
						alert("Please Enter Your Comment");
					}
					
				});
			});
			
			// email submit
			$("#email_chkmodel").ready(function(){
				$("#submitemail").click(function(){
					if($('#inputUserEmail').val() != ''){
						email = $('#inputUserEmail').val();
						$.post("insert_email.php",{email_id:email});
						$('#email_chkmodel').modal('hide');
					}
					else{
						alert("Please Enter Your Email");
					}
				});
			});
			
			//model close and redirect on hate
			$('#loveratemodel').on('hidden.bs.modal', function () {
				var next_url = $("#next_url").val();
				var status = $("#lovehatestatus").val();
				if(status == "0"){
					window.location=next_url; 
				}
			});
		});
	</script>
    
    
<!-- get email address -->
	<?php
	   	if(isset($_SESSION['sess_id']))	{
			$qry = "select * from sf_users where user_email ='' and user_id = '".$_SESSION['sess_id']."'";
			//echo $qry;
			$res = db_query($qry);
			if(db_num_rows($res)>0){
	?>
                <!--for modal-->
                <!-- Modal -->
                <form name="email_chkmodal_form" method="post">
                    <div class="modal fade" id="email_chkmodel" tabindex="-1" data-backdrop="false" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class=" modal-backdrop" style="opacity:0.5;"></div>
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                             <h4 class="modal-title">Please provide your email address (We won't spam you).</h4>
                            </div>
                            <div class="modal-body" style="word-wrap: break-word;">
                                <center>
                                    <div class="row">
                                        <input type="text" class="form-control" id="inputUserEmail" placeholder="Email" name="uemail">
                                        <br />
                                        <button type="button" class="btn btn-success" id="submitemail" aria-hidden="true">Submit</button> 
                                    </div>
                                </center>             
                            </div>
                          </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                  </div>
                </div>
                </form><!-- /.modal -->
                <!--for modal-->
	<?php
				
				echo "<script>$('#email_chkmodel').modal('show');</script>";
			}
		}
	?>
    
    
    
<!-- get email address -->

	</body>
</html>