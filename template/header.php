<?php include("classes/config.php"); ?>
<!DOCTYPE html>
<html lang="en">
<!-- start Mixpanel -->
<script type="text/javascript">
(function(f,b){if(!b.__SV){var a,e,i,g;window.mixpanel=b;b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.track_charge people.clear_charges people.delete_user".split(" ");
for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,e,d])};b.__SV=1.2;a=f.createElement("script");a.type="text/javascript";a.async=!0;a.src="//cdn.mxpnl.com/libs/mixpanel-2.2.min.js";e=f.getElementsByTagName("script")[0];e.parentNode.insertBefore(a,e)}})(document,window.mixpanel||[]);
mixpanel.init("6d6c39dbcb08f348c2d834379615a357");

mixpanel.track_links("#homeUploadBtn", "Home page Upload Button");
mixpanel.track_links("#payBronzePlan", "Pick a plan page Bronze Button");
mixpanel.track_links("#paySilverPlan", "Pick a plan page Silver Button");
</script>
<!-- end Mixpanel -->
  <head>
    <title>SoundFan.com - <?php if(!empty($meta_title)) echo $meta_title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">	
	
	<!-- File Upload -->
	<link rel="stylesheet" href="css/bootstrap-fileupload.css">
	<link rel="stylesheet" href="css/bootstrap-fileupload.min.css">
    
    <link rel="stylesheet" href="css/bootstrap.css">
 <!--   <link rel="stylesheet" href="css/bootstrap.min.css">-->
    <link rel="stylesheet" href="css/bootswatch.min.css">
	<link rel="stylesheet" href="css/general.css">
    
    <!-- autp post switch -->
    <link rel="stylesheet" href="css/bootstrap-switch.css" />
    
    <!-- icons -->
    <link href="css/icons.css" rel="stylesheet">
<script src="https://js.braintreegateway.com/v2/braintree.js"></script>
  <!-- FACEBOOK ESSENTIALS -->
	<script>
	  window.fbAsyncInit = function() {
		  FB.init({
			appId      : '1436998323189487',
			cookie     : true,  // enable cookies to allow the server to access 
								// the session
			xfbml      : true,  // parse social plugins on this page
			version    : 'v2.1' // use version 2.1
		  });
	  };
	  // Load the SDK asynchronously
	  (function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	  }(document, 'script', 'facebook-jssdk'));
	
	  function facebookLogin() {
		//console.log('Welcome!  Fetching your information.... ');
	    $("#btn-login").html("Please wait...");
		FB.api('/me', function(response) {
		  var fbid	= response.id;
		  var name 	= response.name;
		  var email = response.email;
		  //alert(response.id);
		  window.location	= "set_user.php?name="+name+"&email="+email+"&fbid="+fbid;
		});
	  }
	</script>
	<!-- FACEBOOK ESSENTIALS -->
    
    <link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/styles.css" media="all" />
  </head>
  <body>
  	<?php include_once("functions.php"); ?>
  	<?php include_once("analyticstracking.php"); ?>
    <div class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        
        
        <div class="navbar-header col-sm-5">
		
			<div class="bar-right">
			<a class="logo pull-left" href="">
				<img width="133" class="img-responsive" src="img/biglogo.jpg" alt="Logo" />
			</a>
			<div class="submit-song mobile">
				<a href=""><i class="fa fa-upload"></i> Submit a Song</a>
			</div>
			</div>
			<div class="bar_left">
          <button data-target="#navbar-main" data-toggle="collapse" type="button" class="navbar-toggle">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
		  </div>
        </div>
        
        
        <div class="navbar-collapse collapse" id="navbar-main">
       
			<ul class="nav navbar-nav navbar-right">
    
                <li><a href=""><i class="fa fa-envelope"></i> &nbsp;&nbsp;&nbsp;Contact Us</a></li>
                <li class="desktop"><a href=""><i class="fa fa-upload"></i> Submit a Song</a></li>
			</ul>
            <!--
				<li>
					<a href="#">Artists</a>
				</li>
				<li>
				  <a href="#">Latest Tracks</a>
				</li>
			</ul>
		-->
        <ul class="nav navbar-nav navbar-right">
            <?php
				if(isset($_SESSION['sess_id']))
				{
					include ("signup_mail.php");				// to senting Signup mail
					
					echo "<li><center>";
					if(!empty($_SESSION['fbid'])){ 
						echo "<img src='https://graph.facebook.com/".$_SESSION['fbid']."/picture?type=square' width='40' style='padding-top:10px;'/> ";
                   	} else if(!empty($_SESSION['access_token'])){ 
						echo "<img src='".$_SESSION[twitter_profile_pic]."' width='40' style='padding-top:10px;'/> ";
					} 
					echo "</center></li>";
					echo '<li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account <b class="caret"></b></a>
                      <ul class="dropdown-menu">
                        <li><a href="my_songs.php"><i class="icon-music"></i> My Songs</a></li>
                        <li><a href="settings.php"><i class="icon-wrench"></i> Settings</a></li>
                        <li class="divider"></li>
                        <li><a href="logout.php"><i class="icon-off"></i> Logout</a></li>
                      </ul>
                    </li>';
				}
				else
				{
			?>
            		<!--  <li><a href="#" onclick="window.open('FacebookLogin.php?&location=index.php','_blank','top=200,left=415,width=500,height=300,resizable=no', true)"><img src="images/fb.png"></a></li> -->
            		<li>
                    	<a>
	            		<div id="btn-login">
	                    	<fb:login-button max_rows="1" size="large" show_faces="false" auto_logout_link="false" scope="public_profile,email" onlogin="facebookLogin();">
		                    </fb:login-button>
		                </div>
                        </a>
	                </li>
                    <li><a href="login-twitter.php"><img src="images/twitter.png"></a></li>
                    <!-- <li><a href="google/plus/index.php"><img src="images/google.png" width="75"></a></li> -->
            <?php
				}
			?>
			<!--<li>
				<form class="navbar-form navbar-left">
					<input type="text" class="form-control col-lg-8" placeholder="Search">
				</form>
			</li>-->
		</ul>
        </div>
      </div>
    </div>
