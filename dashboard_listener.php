<?php 
	$len 	= 10; 	
	$start 	= (!empty($_GET[page])) ? (($_GET[page]-1)*$len) : 0;
?>
<?php
	include("template/header.php");
?>
<style>
.thumbnail{max-height:200px !important; min-height:200px !important;}
</style>
    <div class="container">
	
      <!-- Typography
      ================================================== -->
      <!--
	  <div class="bs-docs-section">
        <div class="row">
          <div class="col-lg-12">
            <div class="page-header">
              <h1 id="type">Typography</h1>
            </div>
          </div>
        </div> -->

        <!-- Headings -->
        <?php
       
			$qry = "SELECT sf_song_details.*,
						   song_feedback.love,
						   song_feedback.hate,
						   DATE_FORMAT(date(sf_song_details.entereddate),'%m/%d/%Y') AS dt
					FROM sf_song_details
					LEFT JOIN
					  (SELECT *,
							  SUM(`hate_status`) AS hate,
							  SUM(`love_status`) AS love
					   FROM sf_song_feedback
					   GROUP BY song_id) AS song_feedback ON song_feedback.song_id = sf_song_details.song_id
					WHERE sf_song_details.song_id NOT in
						(SELECT DISTINCT song_id
						 FROM sf_song_feedback
						 WHERE user_id = '".$_SESSION['sess_id']."')
					  AND status = 'Active'
					ORDER BY sf_song_details.song_id DESC";
					
			$res = db_query($qry);
			
			if(db_num_rows($res)>0)
			{
								
				function get_username($user_id){
					$qry = "select * from sf_users where user_id='".$user_id."'";
					$res = db_query($qry);
					
					if(db_num_rows($res)>0)
					{
						$user = db_fetch_object($res);
						return $user->user_name;
					}
					else
					{
						echo "ADMIN";
					}
					
					return "ADMIN";
				}
			
		?>

        <div class="row">
        	<center>
                <div class="col-lg-1"></div>            
                <div class="col-lg-10">
                	<div class='page-header'>
						<h1 align="left">Dashboard</h1>
                    </div> 
                    <?php
						$qry_user = "	SELECT sf_users.*,
											   song_feedback.love_count,
											   song_feedback.hate_count,
											   IFNULL(userearned.amount, 0) AS earnamt,
											   IFNULL(userdeduted.amount, 0) AS deductamt
										FROM sf_users
										LEFT JOIN
										  (SELECT user_id,
												  sum(love_status) AS love_count,
												  sum(hate_status) AS hate_count,
												  sum(listen_count) AS listen_count
										   FROM sf_song_feedback
										   GROUP BY user_id) AS song_feedback ON song_feedback.user_id = sf_users.user_id
										LEFT JOIN
										  (SELECT user_id, 
										  		  sum(amount) as amount 
										   FROM sf_transaction where flag = 'earn rate'
										   GROUP BY user_id) AS userearned ON userearned.user_id = sf_users.user_id
										LEFT JOIN
										  (SELECT user_id, 
										  		  sum(amount) as amount 
										   FROM sf_transaction where (flag = 'deduct rate' or flag = 'deduct comment' or flag = 'initial')
										   GROUP BY user_id) AS userdeduted ON userdeduted.user_id = sf_users.user_id
										       
										WHERE sf_users.user_id = '".$_SESSION['sess_id']."'";
						//echo $qry_user."<br>";
						
						$res_user = db_query($qry_user);
						
						if(db_num_rows($res_user)>0):
							
							function get_total_songs(){
								$qry = "select count(*) as cnt from sf_song_details where status = 'Active'";
								$res = db_fetch_object(db_query($qry));
								return $res->cnt;
							}
							
							function get_unrated_songs($user_id){
								$qry = "select count(*) as cnt from sf_song_details where status = 'Active' and song_id not in (select song_id from sf_song_feedback where user_id = '".$user_id."') ";
								$res = db_fetch_object(db_query($qry));
								return $res->cnt;
							}
							
							$row_user = db_fetch_object($res_user);
					?>
                        
                        <div class="panel panel-default" style="border: 1px solid #ccc;">
                          <div class='panel-body'>
                        	<div class="well icondashboard" align="center">
                            	<font color="green"> $ <?php echo number_format($row_user->earnamt,2); ?></font><br>
                            	Money Earned
                            </div>
							<!--
                        	<div class="well icondashboard" align="center">
                            	<font color="green"> $ <?php echo number_format($row_user->deductamt,2); ?></font><br>
                            	Total Balance
                            </div>
                            -->
                        	<div class="well icondashboard" align="center">
                            	<font color="blue"><?php echo ($row_user->love_count + $row_user->hate_count ) ; ?></font><br>
                            	Total Songs Rated
                            </div>
                            <!--
                        	<div class="well icondashboard" align="center">
                            	<font color="orange"><?php echo get_total_songs(); ?></font><br>
                            	Total Songs in Queue
                            </div>
                            -->
                        	<div class="well icondashboard" align="center">
                            	<font color="red"><?php echo get_unrated_songs($_SESSION['sess_id']); ?></font><br>
                            	Total Unrated Songs
                            </div>                                 
                          </div>
                       	</div>                      
                    <?php
						endif;
					?>
                   	<div class="panel panel-default" style="border: 1px solid #ccc;">
                    
                    	<div class='panel-body'>
                        		
                            <!-- PAGINATION -->        
                            <?php
								$total	= 	db_num_rows($res);
								
								$qry 	.= 	" LIMIT $start, $len";							
								$res	= 	db_query($qry);

                                $pg		=	$_GET[page]; if($pg==0 || empty($pg)) $pg = 1;
                                $pg		=	$pg-1;
                                $begin	=	$start+1;
                                $end	=	$len*$pg+$len;
                                if($end>=$total) $end = $total;
                                /* to show in the navigation */
                                if(!empty($_GET[page])) $page = $_GET[page]; else $page=1;
                            ?>
                            <div class="row">  
                            <div class="col-md-12 videopagewhitebg">        
                                <? 	
                                    if($total>0){
                                        $link = $_SERVER[PHP_SELF]."?".$qrySTRING;
                                        drawNavigationPlainAdmin($page,$total,$link);
                                    } 
                                ?>
                            </div>
                            </div>
                            <!-- PAGINATION -->
                        
                        	<?php while($row = db_fetch_object($res)){  $song_details = $row; ?>
                            <div class="panel panel-default" style="border: 1px solid #ccc;">
                    			<div class='panel-body'>
                                    <div class="col-lg-3 ">
                                        <input type="hidden" id="lovehatestatus" />
                                        <?php 
                                            if($song_details->song_cover!='' && file_exists($song_details->song_cover))
                                                echo '<img class="thumbnail" src="'.$song_details->song_cover.'">';
                                            else
                                                echo '<img class="thumbnail" src="http://www.placehold.it/300x300/EFEFEF/AAAAAA&amp;text=Song+Cover+Photo" >';
                                        ?>
                                    </div>	
                                    <div class="col-lg-9">
                                        <div class="col-lg-5" align="left">
                                            <br> <a href="http://www.soundfan.com/<?php echo $song_details->song_url ;?> "> Play this Song </a> 
                                            <!--<br> Date Posted : 	<?php echo $song_details->dt; ?>-->
                                            <br> Uploaded By : 	<?php echo get_username($song_details->enteredby); ?> 
                                        </div>
                                        <div class="col-lg-7">
                                        	<!--
                                        	<br /> Love Count: 	<?php echo $song_details->love; ?>
                                            <br /> Hate Count: 	<?php echo $song_details->hate; ?>
                                            -->
                                            <br />	<a href='https://twitter.com/share?url=http://www.soundfan.com/<?php echo $row->song_url; ?>' target='_blank'><b>Share Via Twitter </b></a> | 
                                            		<a href='https://www.facebook.com/sharer/sharer.php?u=http://www.soundfan.com/<?php echo $row->song_url; ?>' target='_blank'><b>Share Via Facebook </b></a>
                                            
                                        </div>
                                 	</div>
                        		</div>
                      		</div>
	
                            <?php } ?>
                            
                            <!-- PAGINATION -->
                            <div class="row">  
                            <div class="col-md-12 videopagewhitebg">        
                                <? 	
                                    if($total>0){
                                        $link = $_SERVER[PHP_SELF]."?".$qrySTRING;
                                        drawNavigationPlainAdmin($page,$total,$link);
                                    } 
                                ?>
                            </div>
                            </div>
                            <!-- PAGINATION -->
                            
                      	</div>
                    </div>
               </div>
        	</center>
      	</div>
        
        <?php
			}
			else
			{
		?>
         <div class="row">
        	<center>
                <div class="col-lg-1"></div>            
                <div class="col-lg-10">
                	<div class='page-header'>
						<h1 align="left">My Songs</h1>
                    </div> 
                    
                   	<div class="panel panel-default" style="border: 1px solid #ccc;">
                    	<div class='panel-body'>
        				<?php	echo "Sorry You Have Not Uploaded Any Songs";	?>
        				</div>
                    </div>
              </div>
          </center>
          </div>
        <?php } ?>
   	</div>

<?php
	include("template/footer.php");
?>