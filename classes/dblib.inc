<?php
	/********************************************************
	Author: Sooraj	/	Version: 1.0	/	Date: 04-june-2013
	*********************************************************/
	if (!isset($DB_DIE_ON_FAIL)) {
		$DB_DIE_ON_FAIL = true;
	}
	
	if (!isset($DB_DEBUG)) {
		$DB_DEBUG = false;
	}
	
	/************************ start to connect and select database $dbname on $dbhost with the pair $dbuser and $dbpass *******************************/
	/*
	function db_pconnect($dbhost, $dbname, $dbuser, $dbpass) {
			global $DB_DIE_ON_FAIL, $DB_DEBUG;
			if (!$dbh = mysql_pconnect($dbhost, $dbuser, $dbpass)) {
				if ($DB_DEBUG) {
					echo "<h2>Can't connect to $dbhost as $dbuser</h2>";
					echo "<p><b>MySQL Error</b>: ", mysql_error();
				} else {
					echo "<h2>Database error encountered</h2>";
				}
				if ($DB_DIE_ON_FAIL) {
					echo "<p>This script cannot continue, terminating.";
					die();
				}
			}
			if (!mysql_select_db($dbname)) {
				if ($DB_DEBUG) {
					echo "<h2>Can't select database $dbname</h2>";
					echo "<p><b>MySQL Error</b>: ", mysql_error();
				} else {
					echo "<h2>Database error encountered</h2>";
				}
				if ($DB_DIE_ON_FAIL) {
					echo "<p>This script cannot continue, terminating.";
					die();
				}
			}
			return $dbh;
		}*/
	
	/*************************** end of connecting and selecting database **********************************/

	function db_connect() {
		$c1 = mysqli_connect("localhost", "soundfanuser", "(f.TT6gJ163Y28i","soundfan") or die("can not connect to database");
		//mysqli_select_db($c1,"soundfan") or die("can not select database");
		return $c1;
	}
	
	function db_disconnect() {
		mysqli_close();
	}
	
	function db_query($query, $debug = false, $die_on_debug = false, $silent = true) {
		global $DB_DIE_ON_FAIL, $DB_DEBUG, $c1;
	
		if ($debug) {
			echo "<pre>" . htmlspecialchars($query) . "</pre>";
			if ($die_on_debug)
				die;
		}
	
		$qid = mysqli_query($c1,$query);
		
		if (!$qid && !$silent) {
			if ($DB_DEBUG) {
				echo "<h2>Can't execute query</h2>";
				echo "<pre>" . htmlspecialchars($query) . "</pre>";
				echo "<p><b>MySQL Error</b>: ", mysql_error();
			} else {
				echo "<h2>2Database error encountered</h2>";
			}
			if ($DB_DIE_ON_FAIL) {
				echo "<p>This script cannot continue, terminating.";
				die();
			}
		}
	
		return $qid;
	}
	
	function db_fetch_array($qid) {
		global $c1;
		return mysqli_fetch_array($c1,$qid);
	}
	
	function db_list_fields($tab1, $c1, $db_n) {
		global $c1;
		return mysqli_list_fields($db_n, $tab1, $c1);
	}
	
	function db_fetch_row($qid) {
		global $c1;
		return mysqli_fetch_row($c1,$qid);
	}
	
	function db_fetch_object($qid) {
		global $c1;
		return mysqli_fetch_object($qid);
	}
	
	function db_num_rows($qid) {
		global $c1;
		return mysqli_num_rows($qid);
	}
	
	function db_affected_rows() {
		global $c1;
		return mysqli_affected_rows($c1);
	}
	
	function db_insert_id() {
		global $c1;
		return mysqli_insert_id($c1);
	}
	
	function db_free_result($qid) {
		global $c1;
		mysqli_free_result($c1,$qid);
	}
	
	function db_num_fields($qid) {
		global $c1;
		return mysqli_num_fields($c1,$qid);
	}
	
	function db_field_name($qid, $fieldno) {
		global $c1;
		return mysqli_field_name($c1,$qid, $fieldno);
	}
	
	function db_data_seek($qid, $row) {
		global $c1;
		if (db_num_rows($qid)) {
			return mysqli_data_seek($c1,$qid, $row);
		}
	}
//-------------------- PAGINATION -------------------------------------
	function drawNavigationPlainAdmin($page,$total,$link){
		
		print "<ul class='pagination'>";
		
		global $len,$page;
		$check	=	$total%$len;
		$sh		=	$page+9;
		$sv		=	$sh-9;
		
		
		if($check >=1 ) $total_pages=intval(($total/$len))+1; 
		else $total_pages=intval($total/$len);
		
		//if($total>$len) print "Pages : ";

		if($page>1) {$previous_page=$page-1;print "<li><a href='$link&page=$previous_page'>&laquo;</a></li>";}
		else print " &nbsp;&nbsp;";
		
		for($i=1;$i<=$total_pages;$i++)
		{
			if($i>=$sv-3 && $i<=$sh)
			{
				if($page!="$i")
					//print "<a href='$link&page=$i' class='but_02'>$i </a>&nbsp;&nbsp; ";
					print "<li><a href='$link&page=$i'>$i</a></li>";
				else if($page==$i)
					print "<li class='active'><a href='#'>$i</a></li>";
			}
		}
		
		if($total_pages>$page){$nextpage=$page+1; print "<li><a href='$link&page=$nextpage'>&raquo;</a></li>";}
		else print " &nbsp;&nbsp;";
		
		print "</ul>";
	}
	//----------------------------------------------------------------------
?>
