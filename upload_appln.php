<?php

	include("classes/config.php");
	
	class upload_appln{
		
		private $song_id;
		private $song_name;
		private $song_url;
		private $artist_name;
		private $download_status;
		private $budget_id;
		private $contact_number;
		
		function __construct(){
			
			$this->song_name		=	$_POST['song_name'];
			$this->artist_name		=	$_POST['artist_name'];
			$this->song_url			=	$_POST['song_name']."-".$_POST['artist_name'];
			$this->budget_id 		= 	$_POST['promo_budget'];
			$this->contact_number	=	$_POST['uphone'];
			
			// seperator for url clean
			$seperators = array('!','@','#','$','%','^','&','*','(',')','=','<','>','?',':','.','/',',',' ');
			foreach($seperators as $char)
				$this->song_url = strtr($this->song_url,$char,"_");

			$this->song_url = explode("_",$this->song_url);
			$this->song_url = array_filter($this->song_url);
			if(sizeof($this->song_url) >= 1)
				$this->song_url = implode("_",$this->song_url);	
			else
				$this->song_url = "_";
			//seperator for url clean
		
			$this->song_url .= ".html";
			
			if(!empty($_POST['allow_user']))
				$this->download_status	=	"yes";
			else
				$this->download_status	=	"no";				
				
			if($this->check_status()){
			
				$this->song_id		=	$this->submit_todb();
				
				@mkdir("audio/".$this->song_id);
					
				$this->upload_song();
				$this->upload_cover();	
				
				$payid = $this->song_recurring_payment();
				
				header('location:pay.php?payid='.$payid);					// TO REDIRECT TO PAYMENT PAGE
				exit;
				
				//header('location:upload2.php?song_id='.$this->song_id);	// TO REDIRECT TO SHARE SONG
				
			}
			else{
				$_SESSION['upload_error'] = "ERROR!! Sorry Same Song Name Exist!!! ";
				header("location:upload.php");
			}
		}
		
		function check_status(){
		
			$qry = "select * from sf_song_details where song_name = '".$this->song_name."' and song_url='".$this->song_url."' and status = 'active' ";
			if(db_num_rows(db_query($qry)) > 0){
				return false;
			}
			else{
				return true;
			}
		}

		function submit_todb(){
			
			$qry = "INSERT INTO sf_song_details (
						`song_name`,
						`artist_name`,
						`song_url`,
						`download_status`,
						`enteredby`,
						`contact_number`,
						`budget_id`
					) VALUES (
						'".$this->song_name."',
						'".$this->artist_name."',
						'".$this->song_url."',
						'".$this->download_status."',
						'".$_SESSION['sess_id']."',
						'".$this->contact_number."',
						'".$this->budget_id."'
					)";
			
		//	echo $qry;	
						
			$res = db_query($qry);
			
			return db_insert_id();
		}
		
		function upload_cover(){
			
			$path 	= $_FILES['song_cover']['name'];
			$ext 	= pathinfo($path, PATHINFO_EXTENSION);
			
			$cover_url	= "audio/".$this->song_id."/cover.".$ext;
			
			@move_uploaded_file($_FILES['song_cover']['tmp_name'],$cover_url);
			
			$res = db_query("UPDATE sf_song_details set song_cover = '".$cover_url."' where song_id='".$this->song_id."'");
		}
		
		function upload_song(){
			
			$path 	= $_FILES['song_path']['name'];
			$ext 	= pathinfo($path, PATHINFO_EXTENSION);
			
			$song_path	= "audio/".$this->song_id."/audio.".$ext;
			
			@move_uploaded_file($_FILES['song_path']['tmp_name'],$song_path);
			
		/*	if ($_FILES['song_path']['error'] === UPLOAD_ERR_OK) { 
				//uploading successfully done 
			} else { 
				throw new UploadException($_FILES['song_path']['error']); 
			} 
		*/
			$res = db_query("UPDATE sf_song_details set song_path = '".$song_path."' where song_id='".$this->song_id."'");
		}
		
		function song_recurring_payment(){
			$qry = "INSERT INTO sf_song_payment SET user_id = '".$_SESSION['sess_id']."',song_id = '".$this->song_id."', sess_id = '".session_id()."'";
			$res = db_query($qry);
			return db_insert_id();
		}
	}
	
	
	class UploadException extends Exception { 
		public function __construct($code) { 
			$message = $this->codeToMessage($code); 
			parent::__construct($message, $code); 
		} 
	
		private function codeToMessage($code) {
			 
			switch ($code) { 
				case UPLOAD_ERR_INI_SIZE: 
					$message = "The uploaded file exceeds the upload_max_filesize directive in php.ini"; 
					break; 
				case UPLOAD_ERR_FORM_SIZE: 
					$message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form"; 
					break; 
				case UPLOAD_ERR_PARTIAL: 
					$message = "The uploaded file was only partially uploaded"; 
					break; 
				case UPLOAD_ERR_NO_FILE: 
					$message = "No file was uploaded"; 
					break; 
				case UPLOAD_ERR_NO_TMP_DIR: 
					$message = "Missing a temporary folder"; 
					break; 
				case UPLOAD_ERR_CANT_WRITE: 
					$message = "Failed to write file to disk"; 
					break; 
				case UPLOAD_ERR_EXTENSION: 
					$message = "File upload stopped by extension"; 
					break; 
		
				default: 
					$message = "Unknown upload error"; 
					break; 
			} 
			return $message; 
		} 
	} 


	if(isset($_SESSION['sess_id'])){
	
		$obj = new upload_appln();
	
	}
?>