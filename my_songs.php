<?php
	include("template/header.php");
?>
<style>
.thumbnail{max-height:200px !important; min-height:200px !important;}
</style>
    <div class="container">
	
      <!-- Typography
      ================================================== -->
      <!--
	  <div class="bs-docs-section">
        <div class="row">
          <div class="col-lg-12">
            <div class="page-header">
              <h1 id="type">Typography</h1>
            </div>
          </div>
        </div> -->

        <!-- Headings -->
        <?php
       
			$qry = "select *,DATE_FORMAT(date(entereddate),'%m/%d/%Y') as dt from sf_song_details where enteredby = '".$_SESSION['sess_id']."' and status = 'Active' order by song_id desc";
			$res = db_query($qry);
			
			if(db_num_rows($res)>0)
			{
								
				function get_username($user_id){
					
					/*
					$qry = "select * from sf_users where user_id='".$user_id."'";
					$res = db_query($qry);
					
					if(db_num_rows($res)>0)
					{
						$user = db_fetch_object($res);
						return $user->uname;
					}
					else
					{
						echo "ADMIN";
					}
					*/
					
					return "admin";
				}
			
		?>

        <div class="row">
        	<center>
                <div class="col-lg-1"></div>            
                <div class="col-lg-10">
                	<div class='page-header'>
						<h1 align="left">My Songs</h1>
                    </div> 
                    
                   	<div class="panel panel-default" style="border: 1px solid #ccc;">
                    	<div class='panel-body'>
                        
                        	<?php while($row = db_fetch_object($res)){ ?>
                                <div class="col-lg-3">
                                    <?php 
										echo "<a href='listen.php?song_id=".$row->song_id."'>";
                                        if($row->song_cover!='' && file_exists($row->song_cover))
                                            echo '<img class="thumbnail" src="'.$row->song_cover.'">';
                                        else
                                            echo '<img class="thumbnail" src="http://www.placehold.it/300x300/EFEFEF/AAAAAA&amp;text=Song+Cover+Photo" >';
										echo "</a>";

										echo "<a href='listen.php?song_id=".$row->song_id."'><h4>".$row->song_name."</h4></a>";
										echo "Date Posted: ".$row->dt;
										echo "<br><a href='stat.php?song_id=".$row->song_id."'> View Stats </a>";
										echo "<br><br><br>";
                                    ?>
                                </div>	
                            <?php } ?>
                      	</div>
                    </div>
               </div>
        	</center>
      	</div>
        
        <?php
			}
			else
			{
		?>
         <div class="row">
        	<center>
                <div class="col-lg-1"></div>            
                <div class="col-lg-10">
                	<div class='page-header'>
						<h1 align="left">My Songs</h1>
                    </div> 
                    
                   	<div class="panel panel-default" style="border: 1px solid #ccc;">
                    	<div class='panel-body'>
        				<?php	echo "Sorry You Have Not Uploaded Any Songs";	?>
        				</div>
                    </div>
              </div>
          </center>
          </div>
        <?php } ?>
   	</div>
    
    <!--for modal-->
	<!-- Modal -->
    <form name="feedback_form" method="post">
	  	<div class="modal fade" id="feedbackmodel" tabindex="-1" data-backdrop="false" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      		<div class=" modal-backdrop" style="opacity:0.5;"></div>
			<div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                 
                 <h4 class="modal-title">Leave feedback for the Artist</h4>
                </div>
                <div class="modal-body" style="word-wrap: break-word;">
                    <center>
                        This artist would love to have some feedback on their song. Let us know if you "Love it" or "Hate it".
                        <br /> <br />                  
                        <div class="row">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-3">
                                <button type="button" class="btn btn-success"  data-dismiss="modal" id="love_song" value="<?php echo $_GET['song_id']; ?>" aria-hidden="true">Love It</button> 
                            </div>
                            <div class="col-lg-2"></div>
                            <div class="col-lg-3">                        
                                <button type="button" class="btn btn-danger"  data-dismiss="modal" id="hate_song" value="<?php echo $_GET['song_id']; ?>" aria-hidden="true">Hate It</button>
                            </div>
                        </div>
                    </center>             
                </div>
                <div class="modal-footer">
                <!--
                    <div class="row">
                    	<span id="buy_download">
                            <div class="col-lg-5">
                           		<input type="checkbox" id="buy_it" /> I Would Buy This Song
                            </div>
                            <div class="col-lg-1"></div>
                            <div class="col-lg-5">
                            	<input type="checkbox" id="download_it" /> I Would Download This Song	                
                            </div>			
                        </span>
                    <textarea class="form-control" id="comment"></textarea>
                    <br />
                    <button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Submit</button>
                -->
                </div>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
	  </div>
    	</div>
    </form><!-- /.modal -->
	<!--for modal-->

 

<?php
	include("template/footer.php");
?>