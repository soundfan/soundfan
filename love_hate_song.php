<?php

	include("classes/config.php");
	
	class love_hate_song{
		
		private $song_id;
		private $love_it;
		private $hate_it;
		private $user_id;
		
		function __construct(){
			
			$this->song_id	=	$_POST['song_id'];
			
			$this->love_it	=	($_POST['love'] == 0) ? "0" : "1";
			$this->hate_it	=	($_POST['hate'] == 0) ? "0" : "1";
			
			$this->user_id 	=	(isset($_SESSION[sess_id])) ? $_SESSION[sess_id] : "0";
			
			$this->submit_todb();
		}
		
		function submit_todb(){
			$qry = "INSERT INTO  `soundfan`.`sf_song_feedback` (
						`song_id` ,
						`user_id` ,
						`hate_status` ,
						`love_status`,
						`session_id`
					) VALUES (
						'".$this->song_id."',  
						'".$this->user_id."',  
						'".$this->hate_it."',  
						'".$this->love_it."',
						'".session_id()."'
					); ";
			db_query($qry);
			
			//insert to listener
			db_query("insert into sf_transaction set user_id = '$this->user_id', song_id = '$this->song_id', amount = '0.05', comment='Amount earned by rating a track', flag = 'earn rate'  ");
			db_query("insert into sf_transaction set user_id = '".$this->get_track_owner($this->song_id)."', song_id = '$this->song_id', amount = '-0.50', comment='Amount dedeuct on rating a track', flag = 'deduct rate'  ");
						
			$qry = "SELECT *, SUM(  `hate_status` ) AS hate, SUM(  `love_status` ) AS love FROM sf_song_feedback WHERE song_id = '".$this->song_id."' ";

			$res = db_query($qry);
			
			if(db_num_rows($res)>0)
			{
				$row_songdetails = db_fetch_object($res);
				
				$hate  	= ( $row_songdetails->hate == NULL )? 0 : $row_songdetails->hate;
				$love  	= ( $row_songdetails->love == NULL )? 0 : $row_songdetails->love;
			}
			echo '    
			Thanks for your rating. Start earning money for each song you leave feedback by logging in with Facebook. You agreed with '.$love.' other people and loved this song			
			<br /> <br />                              
			<div class="row">
			<div class="col-lg-1"></div>
			<div class="col-lg-5">
				<div class="well">
					<h1 align="center" style="color:#4DB848"><strong>'.$love.'</strong></h1>
					<p align="center">Love Count</p>
				</div> 
			</div>

			<div class="col-lg-5">                        
				<div class="well">
					<h1 align="center" style="color:#ff4056"><strong>'.$hate.'</strong></h1>
					<p align="center">Hate Count</p>
				</div>
			</div>
			</div>';
		}
		
		function get_track_owner($song_id){
			$qry = "select enteredby from sf_song_details where song_id = '".$song_id."'";
			$res = db_query($qry);
			if(db_num_rows($res)>0){
				$row = db_fetch_object($res);
				return $row->enteredby;
			}
			return 0;
		}
	}
	
	//if(isset($_SESSION['userid'])){
	
		$obj = new love_hate_song();
	
	//}
?>