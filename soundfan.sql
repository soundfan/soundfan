-- MySQL dump 10.13  Distrib 5.5.42, for Linux (x86_64)
--
-- Host: localhost    Database: soundfan
-- ------------------------------------------------------
-- Server version	5.5.42

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `soundfan`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `soundfan` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `soundfan`;

--
-- Table structure for table `sf_admin`
--

DROP TABLE IF EXISTS `sf_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sf_admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_name` varchar(155) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_password` varchar(200) NOT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sf_admin`
--

LOCK TABLES `sf_admin` WRITE;
/*!40000 ALTER TABLE `sf_admin` DISABLE KEYS */;
INSERT INTO `sf_admin` VALUES (1,'Brian Dixon','06dixon@gmail.com','test123');
/*!40000 ALTER TABLE `sf_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sf_song_budget`
--

DROP TABLE IF EXISTS `sf_song_budget`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sf_song_budget` (
  `budget_id` int(11) NOT NULL AUTO_INCREMENT,
  `budget` text NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`budget_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sf_song_budget`
--

LOCK TABLES `sf_song_budget` WRITE;
/*!40000 ALTER TABLE `sf_song_budget` DISABLE KEYS */;
INSERT INTO `sf_song_budget` VALUES (1,'No Budget','Active'),(2,'$10','Active'),(3,'$50','Active'),(4,'$100','Active'),(5,'$500+','Active');
/*!40000 ALTER TABLE `sf_song_budget` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sf_song_details`
--

DROP TABLE IF EXISTS `sf_song_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sf_song_details` (
  `song_id` int(11) NOT NULL AUTO_INCREMENT,
  `song_name` varchar(255) NOT NULL,
  `song_url` text NOT NULL,
  `artist_name` varchar(155) NOT NULL,
  `download_status` varchar(50) NOT NULL,
  `enteredby` int(11) NOT NULL,
  `contact_number` varchar(25) NOT NULL,
  `budget_id` int(11) NOT NULL,
  `song_cover` varchar(255) NOT NULL,
  `song_path` varchar(255) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'inactive',
  `entereddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `soundcloud_url` text NOT NULL,
  `source` varchar(55) NOT NULL DEFAULT 'uploaded',
  `sc_user_email` varchar(255) NOT NULL,
  PRIMARY KEY (`song_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sf_song_details`
--

LOCK TABLES `sf_song_details` WRITE;
/*!40000 ALTER TABLE `sf_song_details` DISABLE KEYS */;
INSERT INTO `sf_song_details` VALUES (1,'test song','test_song-srj.html','srj','yes',2,'+1 (333) 333-3333',1,'audio/1/cover.jpg','audio/1/audio.mp3','active','2015-08-13 11:12:04','','uploaded',''),(4,'New song by srj','New_song_by_srj-srj.html','srj','yes',4,'+1 (555) 555-5555',2,'audio/4/cover.jpg','audio/4/audio.mp3','active','2015-08-14 11:16:04','','uploaded',''),(5,'Home song','Home_song-srj.html','srj','yes',4,'+1 (222) 222-2222',2,'audio/5/cover.jpg','audio/5/audio.mp3','active','2015-08-15 04:47:23','','uploaded',''),(7,'song 20-8','song_20-8-srj.html','srj','yes',2,'+1 (666) 666-6666',1,'audio/7/cover.jpg','audio/7/audio.mp3','active','2015-08-20 08:48:05','','uploaded',''),(8,'Test twitter song','Test_twitter_song-srj.html','srj','yes',4,'+1',3,'audio/8/cover.jpg','audio/8/audio.mp3','active','2015-08-20 15:58:12','','uploaded',''),(12,'','Richarddwaynebrown.html','','',0,'',0,'','','active','2015-08-23 02:16:46','Https://www.soundclick.com/Richarddwaynebrown','sc','makhar73@gmail.com'),(13,'She got me','She_got_me-Richard_Dwayne_Brown.html','Richard Dwayne Brown','yes',7,'+1 (478) 354-1962',1,'audio/13/cover.jpg','audio/13/audio.mp3','inactive','2015-08-23 02:23:25','','uploaded',''),(14,'','fear-is-a-liar-the-whappit-mixtape.html','','',0,'',0,'','','active','2015-08-23 07:01:32','http://unkletutu.bandcamp.com/album/fear-is-a-liar-the-whappit-mixtape','sc','whappit@gmail.com'),(15,'','cleaning-out-my-closet.html','','',0,'',0,'','','active','2015-08-23 17:46:31','soundcloud.com/kid-h-sour/sets/cleaning-out-my-closet','sc','kidhsour@hotmail.com'),(16,'','EWTMbByw7Cc.html','','',0,'',0,'','','active','2015-08-24 03:42:06','https://youtu.be/EWTMbByw7Cc','sc','wicketshxtjuggalo@gmail.com'),(17,'','EWTMbByw7Cc.html','','',0,'',0,'','','active','2015-08-24 03:42:38','https://youtu.be/EWTMbByw7Cc','sc','wicketshxtjuggalo@gmail.com'),(18,'','the-end-to-the-pain.html','','',0,'',0,'','','active','2015-08-24 03:44:03','Listen to The End...... (To the Pain) by FreekShitStudios #np on #SoundCloud https://soundcloud.com/freekshitstudios/the-end-to-the-pain','sc','wicketshxtjuggalo@gmail.com'),(19,'','the-end-to-the-pain.html','','',0,'',0,'','','active','2015-08-24 03:44:30','https://soundcloud.com/freekshitstudios/the-end-to-the-pain','sc','wicketshxtjuggalo@gmail.com'),(21,'','drake-back-to-back-freestyle.html.html','','',0,'',0,'','','active','2015-08-25 08:11:16','http://www.soundfan.com/drake-back-to-back-freestyle.html','sc','06dixon@gmail.com'),(24,'','iheart-memphis-hit-the-quan.html','','',0,'',0,'','','active','2015-08-25 09:30:09','https://soundcloud.com/bucknastyent/iheart-memphis-hit-the-quan','sc','06dixon@gmail.com'),(25,'','iheart-memphis-hit-the-quan.html','','',0,'',0,'','','active','2015-08-25 09:46:15','https://soundcloud.com/bucknastyent/iheart-memphis-hit-the-quan','sc','bdixon4@babson.edu'),(26,'','.html','','',0,'',0,'','','active','2015-08-26 08:13:25','','sc',''),(27,'','jeffery-lynn-mandingo.html','','',0,'',0,'','','active','2015-08-26 18:55:53','https://www.hiphopvip.com/song/raphenom/jeffery-lynn-mandingo','sc','staff@raphenom.com'),(28,'','unusual-piratecity.html','','',0,'',0,'','','active','2015-08-28 09:02:40','https://soundcloud.com/wipeout-3/unusual-piratecity','sc','tyrencep@yahoo.com'),(29,'','watch?v=xwdawd80CQ0.html','','',0,'',0,'','','active','2015-08-29 01:48:16','https://www.youtube.com/watch?v=xwdawd80CQ0','sc','Lumbanyam73@gmail.com'),(30,'','bbtm-leak.html','','',0,'',0,'','','active','2015-08-29 19:15:45','https://soundcloud.com/theweeknd/bbtm-leak','sc','06dixon@gmail.com'),(31,'','where-was-you-prod-by-stayhigh149.html','','',0,'',0,'','','active','2015-08-30 07:19:06','https://soundcloud.com/richdreamtitus/where-was-you-prod-by-stayhigh149','sc','richdreamstitus@gmail.com'),(32,'','where-was-you-prod-by-stayhigh149.html','','',0,'',0,'','','active','2015-08-30 07:33:46','https://soundcloud.com/richdreamtitus/where-was-you-prod-by-stayhigh149','sc','richdreamstitus@gmail.com'),(33,'','mixtape.php?tape=104512#.VeOKhenPZHg.html','','',0,'',0,'','','active','2015-08-30 22:59:04','http://www.mixtapefactory.com/mixtape.php?tape=104512#.VeOKhenPZHg','sc','timauriisospiffy56@gmail.com'),(34,'','mixtape.php?tape=104512#.VeOKhenPZHg.html','','',0,'',0,'','','active','2015-08-30 22:59:24','http://www.mixtapefactory.com/mixtape.php?tape=104512#.VeOKhenPZHg','sc','timauriisospiffy56@gmail.com'),(35,'','i-love-designer-king-nation-new-hit.html','','',0,'',0,'','','active','2015-08-30 23:01:01','https://soundcloud.com/timauriisospiffy56/i-love-designer-king-nation-new-hit','sc','timauriisospiffy56@gmail.com'),(36,'','i-love-designer-king-nation-new-hit.html','','',0,'',0,'','','active','2015-08-30 23:04:30','https://soundcloud.com/timauriisospiffy56/i-love-designer-king-nation-new-hit','sc','timauriisospiffy56@gmail.com'),(37,'Drake ~ Back To Back Freestyle','drake-back-to-back-freestyle.html','octobersveryown','',0,'',0,'','','active','2015-08-31 14:42:51','https://soundcloud.com/octobersveryown/drake-back-to-back-freestyle','sc','soorajsolutino@gmail.com'),(38,'Sweeterman - Drake','sweeterman-drake.html','Gimmedaux','',0,'',0,'','','active','2015-08-31 15:41:56','https://soundcloud.com/gimmedaux/sweeterman-drake','sc','06dixon@gmail.com'),(39,'Drake - Cell phone','drake-cellphonechacha-remix.html','DJ Stripes','',0,'',0,'','','active','2015-08-31 15:42:19','https://soundcloud.com/deejaystripes/drake-cellphonechacha-remix','sc','06dixon@gmail.com'),(40,'SCOOBY100 X AT MY HOUSE FRESTYLE','scooby100-x-at-my-house-frestyle.html','Scooby100','',0,'',0,'','','active','2015-08-31 23:06:45','https://soundcloud.com/scooby100/scooby100-x-at-my-house-frestyle','sc','DONNELLJONES9371@GMAIL.COM'),(41,'Georgia11','georgia11.html','MingusKahn','',0,'',0,'','','active','2015-09-02 17:03:56','georgia11','sc','melmarley912@gmail.com'),(42,'BlasÃ©','blase.html','Dime Store Heist','',0,'',0,'','','active','2015-09-03 18:24:28','https://soundcloud.com/tydollasign/blase','sc','06dixon@gmail.com'),(43,'Crush(í¬ëŸ¬ì‰¬) -  Oasis (Feat. ZICO)','crush-oasis-feat-zico.html','ayekelly','',0,'',0,'','','active','2015-09-04 08:27:08','https://soundcloud.com/l2share10/crush-oasis-feat-zico','sc','06dixon@gmail.com'),(44,'somking out them jrars.mp3','somking-out-them-jrars-mp3.html','Drip Da Don IADO E.N.T','',0,'',0,'','','active','2015-09-05 13:43:30','https://soundcloud.com/drip-da-don/somking-out-them-jrars-mp3','sc','ederingtondennis@gmail.com'),(45,'FUCK U BY Dripdadon','fuck-u-by-dripdadon.html','Drip Da Don IADO E.N.T','',0,'',0,'','','active','2015-09-05 13:44:19','https://soundcloud.com/drip-da-don/fuck-u-by-dripdadon','sc','ederingtondennis@gmail.com');
/*!40000 ALTER TABLE `sf_song_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sf_song_feedback`
--

DROP TABLE IF EXISTS `sf_song_feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sf_song_feedback` (
  `feedback_id` int(11) NOT NULL AUTO_INCREMENT,
  `song_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `feedback` text NOT NULL,
  `hate_status` int(5) NOT NULL,
  `love_status` int(5) NOT NULL,
  `listen_count` int(11) NOT NULL,
  `entereddate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `session_id` varchar(50) NOT NULL,
  PRIMARY KEY (`feedback_id`)
) ENGINE=InnoDB AUTO_INCREMENT=140 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sf_song_feedback`
--

LOCK TABLES `sf_song_feedback` WRITE;
/*!40000 ALTER TABLE `sf_song_feedback` DISABLE KEYS */;
INSERT INTO `sf_song_feedback` VALUES (1,1,0,'',0,0,1,'2015-08-13 17:10:31',''),(2,1,0,'',0,1,0,'2015-08-13 17:10:39','k21d2m6f292s1htpvmjqffrcr7'),(3,1,0,'',0,0,1,'2015-08-13 17:19:33',''),(4,1,0,'',0,0,1,'2015-08-13 17:20:58',''),(5,1,0,'',0,0,1,'2015-08-13 17:22:32',''),(6,1,0,'',0,0,1,'2015-08-13 17:27:37',''),(7,1,0,'',1,0,0,'2015-08-13 17:27:43','vkk1i3j05k6m9vsi294k4hu353'),(8,1,0,'',0,0,1,'2015-08-14 04:41:37',''),(9,1,0,'',0,0,1,'2015-08-14 04:44:37',''),(10,1,0,'',0,0,1,'2015-08-14 04:47:38',''),(11,1,0,'',0,0,1,'2015-08-14 04:50:39',''),(12,1,0,'',0,0,1,'2015-08-14 04:53:40',''),(13,1,0,'',0,0,1,'2015-08-14 04:56:40',''),(14,1,0,'',0,0,1,'2015-08-14 04:59:41',''),(15,1,0,'',0,0,1,'2015-08-14 05:02:41',''),(16,1,0,'',0,0,1,'2015-08-14 05:05:42',''),(17,1,0,'',0,0,1,'2015-08-14 05:08:42',''),(18,1,0,'',0,0,1,'2015-08-14 05:11:43',''),(19,1,0,'',0,0,1,'2015-08-14 05:14:43',''),(20,1,0,'',0,0,1,'2015-08-14 05:17:44',''),(21,1,0,'',0,0,1,'2015-08-14 05:20:44',''),(22,1,0,'',0,0,1,'2015-08-14 05:23:45',''),(23,1,0,'',0,0,1,'2015-08-14 05:26:45',''),(24,1,0,'',0,0,1,'2015-08-14 05:29:48',''),(25,1,0,'',0,0,1,'2015-08-14 05:32:56',''),(26,1,0,'',0,0,1,'2015-08-14 05:35:57',''),(27,1,0,'',0,0,1,'2015-08-14 05:38:59',''),(28,1,0,'',0,0,1,'2015-08-14 05:42:00',''),(29,1,0,'',0,0,1,'2015-08-14 05:45:01',''),(30,1,0,'',0,0,1,'2015-08-14 05:48:03',''),(31,1,0,'',0,0,1,'2015-08-14 05:51:06',''),(32,1,0,'',0,0,1,'2015-08-14 05:54:07',''),(33,1,0,'',0,0,1,'2015-08-14 05:57:08',''),(34,1,0,'',0,0,1,'2015-08-14 06:00:09',''),(35,1,0,'',0,0,1,'2015-08-14 06:03:09',''),(36,1,0,'',0,0,1,'2015-08-14 06:06:10',''),(37,1,0,'',0,0,1,'2015-08-14 06:09:11',''),(38,1,0,'',0,0,1,'2015-08-14 06:12:11',''),(39,1,0,'',0,0,1,'2015-08-14 06:15:12',''),(40,1,0,'',0,0,1,'2015-08-14 06:18:12',''),(41,1,0,'',0,0,1,'2015-08-14 06:21:13',''),(42,1,0,'',0,0,1,'2015-08-14 06:24:14',''),(43,1,0,'',0,0,1,'2015-08-14 06:27:15',''),(44,1,0,'',0,0,1,'2015-08-14 06:30:16',''),(45,1,0,'',0,0,1,'2015-08-14 06:33:16',''),(46,1,0,'',0,0,1,'2015-08-14 06:36:17',''),(47,1,0,'',0,0,1,'2015-08-14 06:39:18',''),(48,1,0,'',0,0,1,'2015-08-14 06:42:18',''),(49,1,0,'',0,0,1,'2015-08-14 06:45:19',''),(50,1,0,'',0,0,1,'2015-08-14 06:48:20',''),(51,1,0,'',0,0,1,'2015-08-14 06:51:21',''),(52,1,0,'',0,0,1,'2015-08-14 06:54:21',''),(53,1,0,'',0,0,1,'2015-08-14 06:57:22',''),(54,1,0,'',0,0,1,'2015-08-14 07:00:23',''),(55,1,0,'',0,0,1,'2015-08-14 07:03:23',''),(56,1,0,'',0,0,1,'2015-08-14 07:06:24',''),(57,1,0,'',0,0,1,'2015-08-14 07:09:25',''),(58,4,4,'',0,0,1,'2015-08-14 11:20:05',''),(59,4,4,'',1,0,0,'2015-08-14 11:20:13','jpvksha5mbl767r9gckuu575r3'),(60,4,4,'',0,0,1,'2015-08-14 11:20:19',''),(61,1,4,'',0,0,1,'2015-08-14 11:23:22',''),(62,5,4,'',0,0,1,'2015-08-15 05:05:00',''),(63,1,2,'',0,0,1,'2015-08-19 05:33:02',''),(64,1,2,'',0,0,1,'2015-08-19 05:33:51',''),(65,4,4,'',0,0,1,'2015-08-19 05:34:48',''),(66,5,4,'',0,0,1,'2015-08-19 05:37:22',''),(67,5,0,'',0,0,1,'2015-08-19 05:37:46',''),(68,5,4,'',0,0,1,'2015-08-19 05:37:51',''),(69,4,4,'',0,0,1,'2015-08-19 05:38:27',''),(70,5,0,'',0,0,1,'2015-08-19 05:39:49',''),(71,4,0,'',0,0,1,'2015-08-19 17:16:28',''),(72,7,2,'',0,0,1,'2015-08-20 08:48:43',''),(73,7,2,'',0,1,0,'2015-08-20 08:49:28','4r91cppph7d9l5hbi8ql75utc4'),(74,8,4,'',0,0,1,'2015-08-20 15:59:08',''),(75,8,4,'',0,1,0,'2015-08-20 15:59:24','481ov1uil8cibv5i2vdbrc0601'),(76,8,2,'',0,0,1,'2015-08-20 16:31:10',''),(77,5,2,'',0,0,1,'2015-08-20 16:31:58',''),(78,7,2,'',0,0,1,'2015-08-20 16:34:25',''),(79,4,2,'',0,0,1,'2015-08-20 16:35:31',''),(80,4,2,'',0,1,0,'2015-08-20 16:35:39','481ov1uil8cibv5i2vdbrc0601'),(81,1,0,'',0,0,1,'2015-08-21 03:16:03',''),(82,4,0,'',0,0,1,'2015-08-21 03:19:04',''),(83,8,0,'',0,0,1,'2015-08-21 03:22:07',''),(84,8,0,'',0,0,1,'2015-08-21 03:22:38',''),(85,7,0,'',0,0,1,'2015-08-21 03:22:52',''),(86,7,0,'',0,0,1,'2015-08-21 03:25:53',''),(87,5,0,'',0,0,1,'2015-08-21 03:28:55',''),(88,5,0,'',0,0,1,'2015-08-21 03:29:25',''),(89,4,0,'',0,0,1,'2015-08-21 03:29:54',''),(90,5,0,'',0,0,1,'2015-08-21 03:32:55',''),(91,5,0,'',0,0,1,'2015-08-21 03:33:24',''),(92,8,0,'',0,0,1,'2015-08-21 03:33:53',''),(93,1,0,'',0,0,1,'2015-08-21 03:34:24',''),(94,4,0,'',0,0,1,'2015-08-21 03:37:25',''),(95,7,0,'',0,0,1,'2015-08-21 03:40:25',''),(96,8,0,'',0,0,1,'2015-08-21 03:43:26',''),(97,5,0,'',0,0,1,'2015-08-21 03:43:57',''),(98,7,0,'',0,0,1,'2015-08-21 03:44:26',''),(99,1,0,'',0,0,1,'2015-08-21 03:47:27',''),(100,5,0,'',0,0,1,'2015-08-21 03:50:28',''),(101,8,0,'',0,0,1,'2015-08-21 03:50:57',''),(102,8,0,'',0,0,1,'2015-08-21 03:51:28',''),(103,4,0,'',0,0,1,'2015-08-21 03:51:59',''),(104,8,0,'',0,0,1,'2015-08-21 03:55:00',''),(105,1,0,'',0,0,1,'2015-08-21 03:55:31',''),(106,7,0,'',0,0,1,'2015-08-21 03:58:32',''),(107,7,0,'',0,0,1,'2015-08-21 04:01:32',''),(108,1,0,'',0,0,1,'2015-08-21 04:04:34',''),(109,1,0,'',0,0,1,'2015-08-21 04:07:35',''),(110,5,0,'',0,0,1,'2015-08-21 04:10:36',''),(111,7,0,'',0,0,1,'2015-08-21 04:11:05',''),(112,4,0,'',0,0,1,'2015-08-21 04:14:06',''),(113,5,0,'',0,0,1,'2015-08-21 04:17:07',''),(114,8,0,'',0,0,1,'2015-08-21 04:17:36',''),(115,8,0,'',0,0,1,'2015-08-21 04:18:07',''),(116,8,0,'',0,0,1,'2015-08-21 04:18:37',''),(117,7,0,'',0,0,1,'2015-08-21 04:19:08',''),(118,1,0,'',0,0,1,'2015-08-21 04:22:09',''),(119,8,0,'',0,0,1,'2015-08-21 04:25:11',''),(120,1,0,'',0,0,1,'2015-08-21 04:25:41',''),(121,4,0,'',0,0,1,'2015-08-21 04:28:42',''),(122,4,0,'',0,0,1,'2015-08-21 04:29:44',''),(123,7,0,'',0,0,1,'2015-08-21 04:32:44',''),(124,7,0,'',0,0,1,'2015-08-21 04:33:12',''),(125,5,0,'',0,0,1,'2015-08-21 04:36:12',''),(126,1,0,'',0,0,1,'2015-08-21 04:36:41',''),(127,1,0,'',0,0,1,'2015-08-21 04:39:41',''),(128,8,0,'',0,0,1,'2015-08-21 04:42:43',''),(129,7,0,'',0,0,1,'2015-08-21 04:43:13',''),(130,5,0,'',0,0,1,'2015-08-21 04:46:13',''),(131,7,0,'',0,0,1,'2015-08-21 04:46:42',''),(132,7,0,'',0,0,1,'2015-08-21 04:49:43',''),(133,8,0,'',0,0,1,'2015-08-21 04:52:44',''),(134,8,0,'',0,0,1,'2015-08-21 04:53:15',''),(135,8,0,'',0,0,1,'2015-08-21 04:53:46',''),(136,1,0,'',0,0,1,'2015-08-21 04:54:17',''),(137,5,0,'',0,0,1,'2015-08-21 04:57:18',''),(138,8,0,'',0,0,1,'2015-08-21 04:57:48',''),(139,1,0,'',0,0,1,'2015-08-21 04:58:19','');
/*!40000 ALTER TABLE `sf_song_feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sf_song_notplay`
--

DROP TABLE IF EXISTS `sf_song_notplay`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sf_song_notplay` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `song_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sf_song_notplay`
--

LOCK TABLES `sf_song_notplay` WRITE;
/*!40000 ALTER TABLE `sf_song_notplay` DISABLE KEYS */;
/*!40000 ALTER TABLE `sf_song_notplay` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sf_song_payment`
--

DROP TABLE IF EXISTS `sf_song_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sf_song_payment` (
  `pay_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `song_id` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `sess_id` varchar(55) NOT NULL,
  `status` varchar(15) NOT NULL,
  PRIMARY KEY (`pay_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sf_song_payment`
--

LOCK TABLES `sf_song_payment` WRITE;
/*!40000 ALTER TABLE `sf_song_payment` DISABLE KEYS */;
INSERT INTO `sf_song_payment` VALUES (1,2,1,1,'ks1t71n17lfvdetdtvofkr0b93','Complete'),(2,5,2,1,'vagiba17p4d1d5fb7au4oh1bq7','Complete'),(3,3,3,1,'vagiba17p4d1d5fb7au4oh1bq7','Complete'),(4,4,4,1,'jpvksha5mbl767r9gckuu575r3','Complete'),(5,4,5,1,'6oikv32vj0srbj0b0m85o25qt7','Complete'),(6,6,6,0,'sm4uq7r4rej3410us50r1shl20',''),(7,2,7,1,'4r91cppph7d9l5hbi8ql75utc4','Complete'),(8,4,8,1,'481ov1uil8cibv5i2vdbrc0601','Complete'),(9,7,13,0,'2ivne07k3v6fbofbqen08r0f57','');
/*!40000 ALTER TABLE `sf_song_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sf_song_plans`
--

DROP TABLE IF EXISTS `sf_song_plans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sf_song_plans` (
  `plan_id` int(11) NOT NULL AUTO_INCREMENT,
  `plan` text NOT NULL,
  `amount` double NOT NULL,
  PRIMARY KEY (`plan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sf_song_plans`
--

LOCK TABLES `sf_song_plans` WRITE;
/*!40000 ALTER TABLE `sf_song_plans` DISABLE KEYS */;
INSERT INTO `sf_song_plans` VALUES (1,'Free',0);
/*!40000 ALTER TABLE `sf_song_plans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sf_temp_song_listen_stat`
--

DROP TABLE IF EXISTS `sf_temp_song_listen_stat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sf_temp_song_listen_stat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `song_id` int(11) NOT NULL,
  `session_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sf_temp_song_listen_stat`
--

LOCK TABLES `sf_temp_song_listen_stat` WRITE;
/*!40000 ALTER TABLE `sf_temp_song_listen_stat` DISABLE KEYS */;
INSERT INTO `sf_temp_song_listen_stat` VALUES (1,0,1,'k21d2m6f292s1htpvmjqffrcr7'),(2,0,1,'k21d2m6f292s1htpvmjqffrcr7'),(3,0,1,'k21d2m6f292s1htpvmjqffrcr7'),(4,0,1,'vkk1i3j05k6m9vsi294k4hu353'),(5,0,1,'jpvksha5mbl767r9gckuu575r3'),(6,0,1,'jpvksha5mbl767r9gckuu575r3'),(7,0,1,'jpvksha5mbl767r9gckuu575r3'),(8,0,1,'jpvksha5mbl767r9gckuu575r3'),(9,0,1,'jpvksha5mbl767r9gckuu575r3'),(10,0,1,'jpvksha5mbl767r9gckuu575r3'),(11,0,1,'jpvksha5mbl767r9gckuu575r3'),(12,0,1,'jpvksha5mbl767r9gckuu575r3'),(13,0,1,'jpvksha5mbl767r9gckuu575r3'),(14,0,1,'jpvksha5mbl767r9gckuu575r3'),(15,0,1,'jpvksha5mbl767r9gckuu575r3'),(16,0,1,'jpvksha5mbl767r9gckuu575r3'),(17,0,1,'jpvksha5mbl767r9gckuu575r3'),(18,0,1,'jpvksha5mbl767r9gckuu575r3'),(19,0,1,'jpvksha5mbl767r9gckuu575r3'),(20,0,1,'jpvksha5mbl767r9gckuu575r3'),(21,0,1,'jpvksha5mbl767r9gckuu575r3'),(22,0,1,'jpvksha5mbl767r9gckuu575r3'),(23,0,1,'jpvksha5mbl767r9gckuu575r3'),(24,0,1,'jpvksha5mbl767r9gckuu575r3'),(25,0,1,'jpvksha5mbl767r9gckuu575r3'),(26,0,1,'jpvksha5mbl767r9gckuu575r3'),(27,0,1,'jpvksha5mbl767r9gckuu575r3'),(28,0,1,'jpvksha5mbl767r9gckuu575r3'),(29,0,1,'jpvksha5mbl767r9gckuu575r3'),(30,0,1,'jpvksha5mbl767r9gckuu575r3'),(31,0,1,'jpvksha5mbl767r9gckuu575r3'),(32,0,1,'jpvksha5mbl767r9gckuu575r3'),(33,0,1,'jpvksha5mbl767r9gckuu575r3'),(34,0,1,'jpvksha5mbl767r9gckuu575r3'),(35,0,1,'jpvksha5mbl767r9gckuu575r3'),(36,0,1,'jpvksha5mbl767r9gckuu575r3'),(37,0,1,'jpvksha5mbl767r9gckuu575r3'),(38,0,1,'jpvksha5mbl767r9gckuu575r3'),(39,0,1,'jpvksha5mbl767r9gckuu575r3'),(40,0,1,'jpvksha5mbl767r9gckuu575r3'),(41,0,1,'jpvksha5mbl767r9gckuu575r3'),(42,0,1,'jpvksha5mbl767r9gckuu575r3'),(43,0,1,'jpvksha5mbl767r9gckuu575r3'),(44,0,1,'jpvksha5mbl767r9gckuu575r3'),(45,0,1,'jpvksha5mbl767r9gckuu575r3'),(46,0,1,'jpvksha5mbl767r9gckuu575r3'),(47,0,1,'jpvksha5mbl767r9gckuu575r3'),(48,0,1,'jpvksha5mbl767r9gckuu575r3'),(49,0,1,'jpvksha5mbl767r9gckuu575r3'),(50,0,1,'jpvksha5mbl767r9gckuu575r3'),(51,0,1,'jpvksha5mbl767r9gckuu575r3'),(52,0,1,'jpvksha5mbl767r9gckuu575r3'),(53,0,1,'jpvksha5mbl767r9gckuu575r3'),(54,0,1,'jpvksha5mbl767r9gckuu575r3'),(55,4,4,'jpvksha5mbl767r9gckuu575r3'),(56,4,4,'jpvksha5mbl767r9gckuu575r3'),(57,4,1,'jpvksha5mbl767r9gckuu575r3'),(58,4,5,'6oikv32vj0srbj0b0m85o25qt7'),(59,2,1,'q6sn4ik8m01m9pfms0dkfc3n55'),(60,2,1,'q6sn4ik8m01m9pfms0dkfc3n55'),(61,4,4,'q6sn4ik8m01m9pfms0dkfc3n55'),(62,4,5,'q6sn4ik8m01m9pfms0dkfc3n55'),(63,0,5,'9mkc7r5m7etk830is5cdi1ue05'),(64,4,5,'q6sn4ik8m01m9pfms0dkfc3n55'),(65,4,4,'q6sn4ik8m01m9pfms0dkfc3n55'),(66,0,5,'9mkc7r5m7etk830is5cdi1ue05'),(67,0,4,'fdo5cl7aqaa3v99i15sp47jcj4'),(68,2,7,'4r91cppph7d9l5hbi8ql75utc4'),(69,4,8,'481ov1uil8cibv5i2vdbrc0601'),(70,2,8,'481ov1uil8cibv5i2vdbrc0601'),(71,2,5,'481ov1uil8cibv5i2vdbrc0601'),(72,2,7,'481ov1uil8cibv5i2vdbrc0601'),(73,2,4,'481ov1uil8cibv5i2vdbrc0601'),(74,0,1,'405f89die8su1idpk5l5fe5160'),(75,0,4,'405f89die8su1idpk5l5fe5160'),(76,0,8,'405f89die8su1idpk5l5fe5160'),(77,0,8,'405f89die8su1idpk5l5fe5160'),(78,0,7,'405f89die8su1idpk5l5fe5160'),(79,0,7,'405f89die8su1idpk5l5fe5160'),(80,0,5,'405f89die8su1idpk5l5fe5160'),(81,0,5,'405f89die8su1idpk5l5fe5160'),(82,0,4,'405f89die8su1idpk5l5fe5160'),(83,0,5,'405f89die8su1idpk5l5fe5160'),(84,0,5,'405f89die8su1idpk5l5fe5160'),(85,0,8,'405f89die8su1idpk5l5fe5160'),(86,0,1,'405f89die8su1idpk5l5fe5160'),(87,0,4,'405f89die8su1idpk5l5fe5160'),(88,0,7,'405f89die8su1idpk5l5fe5160'),(89,0,8,'405f89die8su1idpk5l5fe5160'),(90,0,5,'405f89die8su1idpk5l5fe5160'),(91,0,7,'405f89die8su1idpk5l5fe5160'),(92,0,1,'405f89die8su1idpk5l5fe5160'),(93,0,5,'405f89die8su1idpk5l5fe5160'),(94,0,8,'405f89die8su1idpk5l5fe5160'),(95,0,8,'405f89die8su1idpk5l5fe5160'),(96,0,4,'405f89die8su1idpk5l5fe5160'),(97,0,8,'405f89die8su1idpk5l5fe5160'),(98,0,1,'405f89die8su1idpk5l5fe5160'),(99,0,7,'405f89die8su1idpk5l5fe5160'),(100,0,7,'405f89die8su1idpk5l5fe5160'),(101,0,1,'405f89die8su1idpk5l5fe5160'),(102,0,1,'405f89die8su1idpk5l5fe5160'),(103,0,5,'405f89die8su1idpk5l5fe5160'),(104,0,7,'405f89die8su1idpk5l5fe5160'),(105,0,4,'405f89die8su1idpk5l5fe5160'),(106,0,5,'405f89die8su1idpk5l5fe5160'),(107,0,8,'405f89die8su1idpk5l5fe5160'),(108,0,8,'405f89die8su1idpk5l5fe5160'),(109,0,8,'405f89die8su1idpk5l5fe5160'),(110,0,7,'405f89die8su1idpk5l5fe5160'),(111,0,1,'405f89die8su1idpk5l5fe5160'),(112,0,8,'405f89die8su1idpk5l5fe5160'),(113,0,1,'405f89die8su1idpk5l5fe5160'),(114,0,4,'405f89die8su1idpk5l5fe5160'),(115,0,4,'405f89die8su1idpk5l5fe5160'),(116,0,7,'405f89die8su1idpk5l5fe5160'),(117,0,7,'405f89die8su1idpk5l5fe5160'),(118,0,5,'405f89die8su1idpk5l5fe5160'),(119,0,1,'405f89die8su1idpk5l5fe5160'),(120,0,1,'405f89die8su1idpk5l5fe5160'),(121,0,8,'405f89die8su1idpk5l5fe5160'),(122,0,7,'405f89die8su1idpk5l5fe5160'),(123,0,5,'405f89die8su1idpk5l5fe5160'),(124,0,7,'405f89die8su1idpk5l5fe5160'),(125,0,7,'405f89die8su1idpk5l5fe5160'),(126,0,8,'405f89die8su1idpk5l5fe5160'),(127,0,8,'405f89die8su1idpk5l5fe5160'),(128,0,8,'405f89die8su1idpk5l5fe5160'),(129,0,1,'405f89die8su1idpk5l5fe5160'),(130,0,5,'405f89die8su1idpk5l5fe5160'),(131,0,8,'405f89die8su1idpk5l5fe5160'),(132,0,1,'405f89die8su1idpk5l5fe5160');
/*!40000 ALTER TABLE `sf_temp_song_listen_stat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sf_transaction`
--

DROP TABLE IF EXISTS `sf_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sf_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `song_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `comment` text NOT NULL,
  `flag` varchar(55) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sf_transaction`
--

LOCK TABLES `sf_transaction` WRITE;
/*!40000 ALTER TABLE `sf_transaction` DISABLE KEYS */;
INSERT INTO `sf_transaction` VALUES (1,4,8,0.05,'Amount earned by rating a track','earn rate'),(2,4,8,-0.5,'Amount dedeuct on rating a track','deduct rate'),(3,4,8,-0.5,'amt deducted for commenting a track','deduct comment'),(4,2,4,0.05,'Amount earned by rating a track','earn rate'),(5,4,4,-0.5,'Amount dedeuct on rating a track','deduct rate'),(6,0,11,-0.5,'amt deducted for commenting a track','deduct comment'),(7,0,14,-0.5,'amt deducted for commenting a track','deduct comment'),(8,0,35,-0.5,'amt deducted for commenting a track','deduct comment'),(9,0,37,-0.5,'amt deducted for commenting a track','deduct comment'),(10,0,40,-0.5,'amt deducted for commenting a track','deduct comment'),(11,0,40,-0.5,'amt deducted for commenting a track','deduct comment'),(12,0,40,-0.5,'amt deducted for commenting a track','deduct comment'),(13,0,39,-0.5,'amt deducted for commenting a track','deduct comment'),(14,0,39,-0.5,'amt deducted for commenting a track','deduct comment'),(15,0,45,-0.5,'amt deducted for commenting a track','deduct comment');
/*!40000 ALTER TABLE `sf_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sf_users`
--

DROP TABLE IF EXISTS `sf_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sf_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_password` varchar(15) NOT NULL,
  `user_state` varchar(50) NOT NULL,
  `user_city` varchar(50) NOT NULL,
  `facebook_id` bigint(11) NOT NULL,
  `twitter_id` bigint(11) NOT NULL,
  `google_id` varchar(50) NOT NULL,
  `fb_autopost` varchar(10) NOT NULL DEFAULT 'Yes',
  `twt_autopost` varchar(10) NOT NULL DEFAULT 'Yes',
  `daily_autopost` varchar(15) NOT NULL DEFAULT 'Yes',
  `twt_oauth_token` varchar(100) NOT NULL,
  `twt_oauth_token_secret` varchar(100) NOT NULL,
  `signup_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `signup_email` varchar(10) NOT NULL DEFAULT 'Not Sent',
  `daily_mail` varchar(50) NOT NULL DEFAULT 'Yes',
  `last_mail_sent_date` varchar(50) NOT NULL,
  `last_auto_post_date` varchar(20) NOT NULL,
  `last_mail_sent_nonuploaded_date` varchar(50) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sf_users`
--

LOCK TABLES `sf_users` WRITE;
/*!40000 ALTER TABLE `sf_users` DISABLE KEYS */;
INSERT INTO `sf_users` VALUES (2,'Sooraj V Nair','','sooraj.maarar@gmail.com','','','',100001725795417,0,'','Yes','Yes','Yes','','','2015-08-12 17:02:34','Sent','Yes','','',''),(3,'Rick Burns','','undefined','','','',641702631,0,'','Yes','Yes','Yes','','','2015-08-13 06:47:54','Not Sent','Yes','','',''),(4,'Sooraj V','soorajmaarar','soorajsolutino@gmail.com','','','',0,483923930,'','Yes','Yes','Yes','483923930-uboijAvIAfc07XgBMtxXy9Rv7KOcgeDaFs54rAup','fgA4zHtTzkTjcuuwN22Z0jE8uQUnpbQRApmL8ysIHf0oh','2015-08-13 12:00:43','Sent','Yes','','',''),(5,'SoundFan','SoundFanMusic','brian@soundfan.com','','','',0,352310443,'','Yes','Yes','Yes','352310443-QFztkUNDJRkEW3vgTXg2r3dS4h3xEHrDEMj4qDhc','1F5gih6pBkrHvvC8K3FsbtriK4ubBCo1Xwir9t7cK3oKM','2015-08-14 08:42:43','Not Sent','Yes','','',''),(6,'Aswin Ramesh','','aswinrames@gmail.com','','','',100005166041079,0,'','No','No','No','','','2015-08-15 15:13:06','Sent','No','','',''),(7,'Richard Dwayne Brown','','makhar73@gmail.com','','','',100003589500512,0,'','Yes','Yes','Yes','','','2015-08-23 02:20:18','Sent','Yes','','',''),(8,'Yung Gage','','spaceythehorse@yahoo.com','','','',100000300934454,0,'','Yes','Yes','Yes','','','2015-08-23 07:02:22','Sent','Yes','','',''),(9,'Raphenom Promotions','raphenom','','','','',0,455413544,'','Yes','Yes','Yes','455413544-TJlrxAKxBIwUlviTjnO8TmsrbUv4Q6qZYt7Wcr0v','RuFfcFZvoWhzSFpqKkvHPBI5GCrwCRo0YU0LA79phIt7A','2015-08-26 18:56:40','Sent','Yes','','',''),(10,'Tyrence Jamar','','tyrencep@yahoo.com','','','',987680307938355,0,'','Yes','Yes','Yes','','','2015-08-28 09:03:45','Not Sent','Yes','','',''),(11,'RichDreams Titus','','undefined','','','',1486515155000058,0,'','Yes','Yes','Yes','','','2015-08-30 07:26:53','Sent','Yes','','',''),(12,'King Nation','','','','','',1656362571268189,0,'','Yes','Yes','Yes','','','2015-08-30 23:02:22','Sent','Yes','','',''),(13,'SmokeMoreThanYamama','RealScooby100','','','Florida, USA','Florida, USA',0,2782363770,'','Yes','Yes','Yes','2782363770-UVp0iMnxBJGF17xFu22dyu184kdb4jM8PNB5t9v','yahs6YDrYoVwiFt8TWNIHUE6VHr2W8skz8jeece5ojTR5','2015-08-31 23:09:23','Sent','Yes','','',''),(14,'Anna  Litvinuk','','inno_end@mail.ru','','','',765773500199102,0,'','Yes','Yes','Yes','','','2015-08-31 23:39:19','Sent','Yes','','',''),(15,'Mel Huncho Marley','','melmarley912@gmail.com','','','',863967637005448,0,'','Yes','Yes','Yes','','','2015-09-02 17:04:44','Not Sent','Yes','','',''),(16,'Dennis G Ederington','','ederingtondennis@yahoo.com','','','',1007873308,0,'','Yes','Yes','Yes','','','2015-09-05 13:43:42','Sent','Yes','','','');
/*!40000 ALTER TABLE `sf_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sf_users_comment_song`
--

DROP TABLE IF EXISTS `sf_users_comment_song`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sf_users_comment_song` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `song_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `love_status` varchar(155) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sf_users_comment_song`
--

LOCK TABLES `sf_users_comment_song` WRITE;
/*!40000 ALTER TABLE `sf_users_comment_song` DISABLE KEYS */;
INSERT INTO `sf_users_comment_song` VALUES (1,4,8,'Nice song','1'),(2,0,11,'sadsada',''),(3,8,14,'Old school ',''),(4,0,35,'King Nation hard af',''),(5,0,37,'asdasdas',''),(6,0,40,'LOVE THE SONG BRO',''),(7,0,40,'LIVE A FUK',''),(8,0,40,'money making hit you really goin places',''),(9,14,39,'Test',''),(10,0,39,'adskhalkdlkajbds lfkasdf',''),(11,16,45,'like share if you love the music','');
/*!40000 ALTER TABLE `sf_users_comment_song` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-08  5:15:39
