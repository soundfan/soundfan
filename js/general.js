// JavaScript Document
var _validFileExtensions = [".mp3", ".m4a", ".mp4", ".ogg", ".wav"];
	
function validate_song_path() {
	
	if(document.form_upload_song.song_path.value != '')
	{
		var sFileName = document.form_upload_song.song_path.value;
		if (sFileName.length > 0) {
			
			for (var j = 0; j < _validFileExtensions.length; j++) {
				var sCurExtension = _validFileExtensions[j];
				if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
					return true;
				}
			}
	
			return false;
		}
	
		return false;
	}
	else
		return false;
}

var upload_file = true;

function chk_upload_song(){

	document.form_upload_song.submit_button.disabled = true;
	if(upload_file)
	{
		upload_file = false;
		
		if(document.form_upload_song.song_name.value != '')
		{
			if(document.form_upload_song.artist_name.value != '')
			{
				if(document.form_upload_song.promo_budget.value != "")
				{
				
					if(validate_song_path())
					{
						document.form_upload_song.submit();
					}
					else
					{
						alert("Sorry, This Song is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
						upload_file = true;
						document.form_upload_song.submit_button.disabled = false;
						document.form_upload_song.song_path.focus();
					}
				}
				else{
					alert("Please chosse a budget");
					upload_file = true;
					document.form_upload_song.submit_button.disabled = false;
					document.form_upload_song.promo_budget.focus();			
				}
			}
			else
			{
				alert("Please enter the artist name");
				upload_file = true;
				document.form_upload_song.submit_button.disabled = false;
				document.form_upload_song.artist_name.focus();			
			}
		}
		else
		{
			alert("Please enter the song title");
			upload_file = true;
			document.form_upload_song.submit_button.disabled = false;			
			document.form_upload_song.song_name.focus();
		}
	}
}

function chk_contact_mail_delete(){
	
	count = $('#maillist :checkbox:checked').length;
		
	if(count > 0){
		document.contact_mail.action = "contact_mail_list_delete.php";
		document.contact_mail.submit();
	}
	else{
		alert("Please Choose atleast one contact");
	}
}

function chk_contact_mail_submit(){
	
	count = $('#maillist :checkbox:checked').length;
		
	if(count > 0){
		document.contact_mail.action = "contact_mail_list_compose.php";
		document.contact_mail.submit();
	}
	else{
		alert("Please Choose atleast one contact");
	}
}

/*		jquery starts		*/
$(document).ready(function(){
	$('#check_all').click(function() {
		var $checkboxes = $("#maillist").find('input[type=checkbox]');
		$checkboxes.prop('checked', $(this).is(':checked'));
	});
});

/*		jquery ends		*/