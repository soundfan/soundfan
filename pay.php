<?php
	include("template/header.php");
?>
    <div class="container">
        <div class="row">
			<div class="col-lg-9">
                <div class="row">
					<div class="col-lg-12">
						<div class='page-header'>
							<h1> Pick a plan </h1>
						</div>
					</div>
				</div>

                <div class="col-xs-12 col-md-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Bronze</h3>
                        </div>
                        <div class="panel-body">
                            <div class="the-price">
                                <h1>Free Plan <!-- <span class="subscript">/mo</span> --> </h1>
                                <!-- <small>1 month FREE trial</small> -->
                            </div>
                            <table class="table">
                                <tr>
                                    <td>
                                        5 people will rate this song but no comments
                                    </td>
                                </tr>
                                <tr class="active">
                                    <td>
                                        No instant alert when someone leaves feedback
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        No set timetable for results
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="panel-footer">
                            <a href="freepayment.php?planid=1&payid=<?php echo $_GET['payid']; ?>" class="btn btn-success" role="button" id="payBronzePlan" data-planid="1">Sign Up</a>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="panel panel-success">
                        <!--
                        <div class="cnrflash">
                            <div class="cnrflash-inner">
                                <span class="cnrflash-label">MOST <br> POPULR</span>
                            </div>
                        </div>
                        -->
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                Silver</h3>
                        </div>
                        <div class="panel-body">
                            <div class="the-price">
                                <h1> $2.99 <span class="subscript">/week</span></h1>
                                <small>1 week FREE trial</small>
                            </div>
                            <table class="table">
                                <tr>
                                    <td>
                                        <b>50</b> people will rate and <b>comment</b> on this song.
                                    </td>
                                </tr>
                                <tr class="active">
                                    <td>
                                        Instant alert when someone leaves feedback
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Delivered in 5 business days!
                                    </td>
                                </tr>
                                <tr class="active">
                                    <td>
                                        100% Money Back Guarantee!
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="panel-footer">
                            <!-- <a href="#" class="btn btn-success" role="button" data-planid="2" data-toggle="modal" data-target="#paymentModal" id="paySilverPlan">Sign Up</a> -->
                            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                            <input type="hidden" name="cmd" value="_s-xclick">
                            <input type="hidden" name="hosted_button_id" value="4EGDJQCFM7WWW">
                            <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                            <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                            </form>

                        </div>
                    </div>
                </div>
        	</div>
        </div>
    </div>
<?php
//	BRIAN TREE PAYMENT SETUP STARTS
	require_once 'braintree-php-2.30.0/lib/Braintree.php'; // live

	Braintree_Configuration::environment('sandbox');
	Braintree_Configuration::merchantId('8nryh5m7tcqsy2vd');
	Braintree_Configuration::publicKey('rgwchq6q3cxf22gc');
	Braintree_Configuration::privateKey('0db4a6b396f0efa7916369c30187506a');

	//live
	//Braintree_Configuration::environment('production');
		//Braintree_Configuration::merchantId('bnkss77t6xzcrf6k');
		//Braintree_Configuration::publicKey('bpsk6h2bywj22khd');
		//Braintree_Configuration::privateKey('41f180ab4d45dd7a1bd93077fd4caf78');

		$clientToken = Braintree_ClientToken::generate(array(
			"customerId" => $_SESSION['sessid']
		));

		$userdetails = db_fetch_object(db_query("SELECT * from sf_users inner join sf_song_payment on sf_users.user_id = sf_song_payment.user_id left join sf_song_details on sf_song_payment.song_id = sf_song_details.song_id where pay_id = '".$_GET['payid']."'"));

		if($userdetails->briantree_customerid == ''){
			$result = Braintree_Customer::create(array(
				'firstName' => $userdetails->user_name,
				'email' => $userdetails->user_email,
			));

			if($result->success){
				db_query("UPDATE sf_users SET briantree_customerid = '".$result->customer->id."' where user_id = '".$userdetails->user_id."'");
			}
		}
		else{

		}

?>
<script src="https://js.braintreegateway.com/v2/braintree.js"></script>
<div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">You will be charged $25 after your free trial</h4>
      </div>
      <div class="modal-body">

    <form id="merchant-form" action="makepayment.php" method="post">
      <div id="paypal-container"></div>
      <input type="hidden" value="25" name="amount" /><br>
      <input type="hidden" value="<?php echo $userdetails->song_url; ?>" name="songurl" />
	  <input type="hidden" value="<?php echo $_GET['payid']; ?>" name="payid" />
      <input type="hidden" value="" name="planid" id="planid" />
      <input type="hidden" value="<?php echo $userdetails->briantree_customerid; ?>" name="customerid" />
      <input type="submit" value="Submit" class="btn btn-primary form-control" />
    </form>

      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>

	braintree.setup("<?php echo $clientToken; ?>","dropin", {
		container: "paypal-container",  // to specify DOM elements, use an ID, a DOM node, or a jQuery element
		  paypal: {
					singleUse: true
				  },
		paymentMethodNonceInputField: "payment-method-nonce"
	});

</script>
<?php
	// BRAIN TREE PAYMENT SETUP ENDS
	include("template/footer.php");
?>

<script>
	$("#paymentModal").on("show.bs.modal",function(e){
		$(this).find('#planid').val($(e.relatedTarget).data('planid'));
	});
</script>