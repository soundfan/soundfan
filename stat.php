<?php
	include("template/header.php");
	
	if(!isset($_SESSION['sess_id']))
	{
		header("location:index.php");
		exit;
	}
?>
    <div class="container">
	
      <!--     
      <div class="page-header" id="banner">
        <div class="row">
          <div class="col-lg-6">
   			 <h1>Flatly</h1>
            <p class="lead">Flat and modern</p> 
          </div>

        </div>
      </div>-->

	
      <!-- Typography
      ================================================== -->
      <!--
	  <div class="bs-docs-section">
        <div class="row">
          <div class="col-lg-12">
            <div class="page-header">
              <h1 id="type">Typography</h1>
            </div>
          </div>
        </div> -->

        <!-- Headings -->
	<div class="row">
    	<div class="col-lg-12">
        <div class='page-header'>
            <h1 align="left">Statistics</h1>
        </div> 
    	<div class="panel panel-primary">
<!--   		<div class="panel-heading">
    			<h3 class="panel-title">Statistics</h3>
    		</div>-->
    	
        <?php
			if(!filter_has_var(INPUT_GET,"song_id"))
			{
				header("location:index.php");
				exit;
			}
			
			
			
			$song_id = $_GET['song_id'];
			
			$qry = "SELECT *, SUM(  `hate_status` ) AS hate, SUM(  `love_status` ) AS love FROM sf_song_feedback WHERE song_id = '".$song_id."' ";

			$res = db_query($qry) or die(mysql_error());
			
			if(db_num_rows($res)>0)
			{
				$row_songdetails = db_fetch_object($res);
				
				$hate  	= ( $row_songdetails->hate == NULL )? 0 : $row_songdetails->hate;
				$love  	= ( $row_songdetails->love == NULL )? 0 : $row_songdetails->love;
			}
			else
			{
				echo "No Song Yet!!!";
				exit;
			}
		?>
    
   
          	<div class="panel-body">
            	<div class="row">
                	<div class="col-sm-12">
						<?php
							$qry_song_details = "select *,DATE_FORMAT(date(entereddate),'%m/%d/%Y') as dt from sf_song_details where song_id = '".$song_id."'";
							$res_song_details = db_query($qry_song_details);
							$row_song = db_fetch_object($res_song_details);
							echo "<h4>".$row_song->artist_name." - ".$row_song->song_name."</h4>";
							echo "Date Posted: ".$row_song->dt;
							echo "<br><br>";
                        ?>
                    </div>
                </div>
                <div class="row">
                <div class="col-sm-1"></div>
                   
                    <div class="col-sm-5">
                        <div class="well">
                            <h1 align="center" style="color:#4DB848"><strong><?php echo $love; ?></strong></h1>
                            <p align="center">Love Count</p>
                        </div>
                    </div>
                    
                    <div class="col-sm-5">
                        <div class="well">
                            <h1 align="center" style="color:#ff4056"><strong><?php echo $hate; ?></strong></h1>
                            <p align="center">Hate Count</p>
                        </div>
                    </div>
            	
                  	<!--  
                    <div class="col-sm-4">
                        <div class="well">
                            <h1 align="center" style="color:#ff4056"><strong>2</strong></h1>
                            <p align="center">Downloads</p>
                        </div>
                    </div>
                    -->
                </div> 
            </div>
  		</div>
        </div>
	</div>
		
    </div>

<?php
	include("template/footer.php");
?>