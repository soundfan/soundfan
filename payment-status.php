<?php
	if(!filter_has_var(INPUT_GET,"txn")){
		header("location:index.php");
		exit;
	}
?>
<?php
	include("template/header.php");
?>
    <div class="container">
        <div class="row">
			<div class="col-lg-12">
				<div class='page-header'>
					<h1> Payment Status </h1>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
            <center>
			<?php
				if($_GET['txn'] == "success" && $_GET['payid'] != '' ):
					
					$qry = "SELECT * , sf_song_payment.status AS pay_status
							FROM  `sf_song_details` 
							INNER JOIN sf_song_payment ON sf_song_payment.song_id = sf_song_details.song_id
							LEFT JOIN sf_song_plans ON sf_song_plans.plan_id = sf_song_payment.plan_id
							WHERE pay_id = '".$_GET['payid']."'";
					//echo $qry;		
					$res = db_query($qry);
					if(db_num_rows($res)>0){
						$row = db_fetch_object($res);
					}
			?>		
					<div class="alert alert-success" role="alert"><strong>Payment Success!</strong></div>
                    <div class="well col-lg-6 col-lg-offset-3" align="left">
                    	<h3> Subscription Details </h3>
                        <ul class="list-group">
                          <li class="list-group-item">Subscripiton Plan 	<span class="label label-warning pull-right"> <?php echo $row->plan; 		?> </span>   </li>
                          <li class="list-group-item">Subscripiton Amount	<span class="label label-warning pull-right"> $ <?php echo $row->amount; 		?> </span>   </li>
                          <li class="list-group-item">Subscripiton Status	<span class="label label-warning pull-right"> <?php echo $row->pay_status;	?> </span>   </li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                    <a class="btn btn-primary btn-lg" style="color:#FFF !important;" href="upload2.php?song_id=<?php echo $row->song_id; ?>"> Continue </a>	<!-- TO REDIRECT TO SHARE SONG -->
			<?php		
				else:
					echo '<div class="alert alert-danger" role="alert"><strong>Payment Unsuccessful!</strong></div>';
				endif;
			?>		
            </center>		
			</div>
		</div>
	</div>
    
<?php
	include("template/footer.php");
?>