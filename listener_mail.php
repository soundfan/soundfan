<?php
	
	require_once("classes/dblib.inc");

	$c1=db_connect();
	
	$q = db_query("	SELECT sf_users . * ,
						   song_feedback . *
					FROM `sf_users`
					INNER JOIN
					  ( SELECT user_id,
							   SUM(love_status) AS love_count,
							   SUM(hate_status) AS hate_count,
							   SUM(listen_count) AS listen_count
					   FROM sf_song_feedback
					   GROUP BY user_id) AS song_feedback ON song_feedback.user_id = sf_users.user_id
					   
					WHERE sf_users.last_listener_mail_sent_date <> '".date('Y-m-d')."' and sf_users.daily_mail = 'Yes' limit 50 ");
					
	$total=db_num_rows($q);

	require_once ("amazon_sdk/sdk.class.php");									// amazon sdk file

	while($r = db_fetch_object($q)){

		$ses = new AmazonSES();
		
		$rQuota = $ses->get_send_quota();
	
		$quota24 = (int) $rQuota->body->GetSendQuotaResult->Max24HourSend;
		$sentCnt = (int) $rQuota->body->GetSendQuotaResult->SentLast24Hours;
	
		if($quota24 > ($sentCnt+$total)) /* 1800 > (1720+40) */
		{
			//print "came inside 1 <br>" ;
			
			if($total>0)
			{
				//print "came inside 2 <br>" ;
				
				$scount = $fcount = 0;
				
				//template Starts
				
				$mailcontent = "Hi ".$r->user_name.", <br><br>Thanks for being an important member of SoundFan community! So far, you have listened to ".($r->listen_count)." songs and rated ".($r->love_count + $r->hate_count)." songs on SoundFan.com. You've loved <font color='green'>".($r->love_count)."</font> of them and hated <font color='red'>".($r->hate_count)."</font> of them. Let us know what you think of these 3 songs today: <br><br>";
				
				
				$mailcontent .= get_contents($r->user_id);
				
				
				$mailcontent .= "<br><br>-SoundFan Team <b>(Reply to this email to reach a real person)</b><br><br>.<a href='http://www.soundfan.com/'>SoundFan</a> • <a href='http://twitter.com/SoundFanMusic'>Twitter</a>•";
				//template Ends
				
				$subject = "SoundFan - You've rated ".($r->love_count + $r->hate_count)." songs so far…..";
				
				$email=$r->user_email;
	
				/* amazon mailing */
	
				$destination = CFComplexType::map(array('ToAddresses'=>$email));
	
				$source = 'team@soundfan.com';
				
				//	$destination = CFComplexType::map(array('ToAddresses'=>$email));
				
				$message = CFComplexType::map(array('Subject.Data'=>$subject, 'Body.Html.Data'=>$mailcontent));
				
				$rSendEmail = $ses->send_email($source, $destination, $message);
				
				if($rSendEmail->status==200) $scount=1;
				else $fcount=1;
				
				$qry_update = db_query("update sf_users set last_listener_mail_sent_date = '".date('Y-m-d')."' where user_id = '".$r->user_id."'");		// updating last mail sent date
				
				
			} /* if total >0 */
	
		}  /* if quota allows */
	}
	
	// creating mail template
	function get_contents($user_id){
		
		
		$qry = "select * from sf_song_details where status='active' order by song_id desc limit 3";
		$res = db_query($qry);
		$str = "";
		
		if(db_num_rows($res)>0)
		{
			while($row = db_fetch_object($res))
			{
				$hate = $love = 0;
				$qry_count = "SELECT *, SUM(  `hate_status` ) AS hate, SUM(  `love_status` ) AS love FROM sf_song_feedback WHERE song_id = '".$row->song_id."' ";
				$res_count = db_query($qry_count) or die(mysql_error());
			
				if(db_num_rows($res_count)>0)
				{
					$row_songdetails = db_fetch_object($res_count);
					
					$hate  	= ( $row_songdetails->hate == NULL )? 0 : $row_songdetails->hate;
					$love  	= ( $row_songdetails->love == NULL )? 0 : $row_songdetails->love;
					$str .= "
						<table cellpadding=10 cellspacing=10 border=1 width='50%' style='border-collapse:collapse;'>
							<tr>
								<td colspan=2>
									<font size='+2'><a href='http://www.soundfan.com/".$row->song_url."'>".$row->artist_name." - ".$row->song_name."</a></font>
								</td> 
							</tr>
							<tr>
								<td align='center'>
									<font color='#4DB848'>".$love."</font><br><b>Love count</b>
								</td>
								<td align='center'>
									<font color='#ff4056'>".$hate."</font><br><b>Hate count</b>
								</td>
							</tr>
							<tr>
								<td align='center'>	
								<a href='https://twitter.com/share?url=http://www.soundfan.com/".$row->song_url."' target='_blank'><b>Share Via Twitter </b></a>
                                </td>

								<td align='center'>	
								<a href='https://www.facebook.com/sharer/sharer.php?u=http://www.soundfan.com/".$row->song_url."' target='_blank'><b>Share Via Facebook </b></a>
                                </td>

							</tr>
						</table><br>
						";
				}
			}
		}
		
		return $str;
	}
?>