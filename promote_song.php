<?php
	include("template/header.php");
	$song_id = "";
	
//	print_r($_GET);
	if(filter_has_var(INPUT_GET,"song_id") && isset($_SESSION['sess_id']))
	{
		$song_id = $_GET['song_id'];
	}
	else
	{
		header('location:index.php');
		exit;
	}
?>
    <div class="container">

      <!-- Typography
      ================================================== -->
      <!--
	  <div class="bs-docs-section">
        <div class="row">
          <div class="col-lg-12">
            <div class="page-header">
              <h1 id="type">Typography</h1>
            </div>
          </div>
        </div> -->

        <!-- Headings -->

	<?php
       $qry = "select *,DATE_FORMAT(date(entereddate),'%m/%d/%Y') as dt from sf_song_details where song_id='".$song_id."' and status = 'active' and enteredby = '".$_SESSION['sess_id']."'";
			$res = db_query($qry);
			
			if(db_num_rows($res)>0)
			{
				$song_details = db_fetch_object($res);
				
				function get_username($user_id){
					
					$qry = "select * from sf_users where user_id='".$user_id."'";
					$res = db_query($qry);
					
					if(db_num_rows($res)>0)
					{
						$user = db_fetch_object($res);
						if(!empty($user->twitter_id))
							return "<a href='https://twitter.com/".$user->screen_name."'>@".$user->screen_name."</a>";
						else if(!empty($user->facebook_id))
							return "<a href='https://facebook.com/profile.php?id=".$user->facebook_id."'>".$user->user_name."</a>";
						else
							return $user->user_name;
					}
					else
					{
						echo "ADMIN";
					}
				}
			
		?>
		<form action="promote_song_appln.php" method="post" name="promote_song">
        <div class="row">
            <div class="col-lg-1"></div>            
            <div class="col-lg-10">
                <div class='page-header'>
                    <h1 align="left">Promote Your Song</h1>
                </div> 
                <div class="row">
	    		   	<div class="col-lg-12">
	                    <h3><?php echo $song_details->artist_name." - ".$song_details->song_name; ?></h3>
                    </div>
             	</div>
        
                <div class="panel panel-default" style="border: 1px solid #ccc;">
                    <div class='panel-body'>
                        <div class="col-lg-12">
                     
                            <div class="row">
                                <div class="col-lg-12">
                                <legend>Enable Auto Tweet Settings</legend>
                                
                                <table cellpadding="7">
                                <tbody>
                                <?php	if(!empty($_SESSION['access_token'])){ 	 ?>
                                <tr><td>Twitter Auto Post</td>
                                <td>
                                    <div class="make-switch switch-small" data-on="success" data-off="danger">
                                     <input type="checkbox" name="twitter_autopost" <?php echo ($song_details->twitter_auto_post == "Yes") ? "checked" : ""; ?> value="" >
                                    </div>
                                </td></tr>
                                <tr><td colspan="2">
                                	This will enable you to post a message on your Twitter Page once a day automatically.
                                </td></tr>
                                <?php } if(!empty($_SESSION['fbid'])){ ?>
                                <tr><td>Facebook Auto Post</td>
                                <td>
                                    <div class="make-switch switch-small" data-on="success" data-off="danger">
                                     <input type="checkbox" name="fb_autopost" <?php echo ($song_details->fb_auto_post == "Yes") ? "checked" : ""; ?> value="">
                                    </div>
                                </td></tr>
                                <tr><td colspan="2">
                                	This will enable you to post a message on your Facebook Page once a day automatically.
                                </td></tr>
                                <?php } ?>
                                </tbody></table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
           </div>
      	</div>
        
        <div class="row">
	        <div class="col-lg-1"></div>
            <div class="col-lg-10">
            	<textarea class="form-control" id="auto_post_mesg" name="auto_post_mesg"><?php echo ($song_details->auto_post_mesg != '')? $song_details->auto_post_mesg: " Listen and rate my song I've uploaded to #soundfan: http://www.soundfan.com/".$song_details->song_url; ?></textarea><br>
            </div>
       	</div>
        
        <div id="row">
        	<div class="col-lg-11" align="right">
            	
             		<input type="hidden" value="<?php echo $song_id; ?>" name="song_id" />
             		<input type="submit" value="Continue" class="btn btn-primary" name="submit_button" />
         			<!--<a href="upload.php" class="btn btn-primary" style='color:#FFF !important;'>Upload Another Song</a>
          			<a href="<?php echo "http://www.soundfan.com/".$song_details->song_url; ?>" class="btn btn-primary" style='color:#FFF !important;'>Visit This Song Page </a>-->
            </div>
        </div>
        </form>
        
        <?php
			}
			else
			{
				header("location:index.php");
			}
		?>
   	</div>
        	</div>
            <!--
            <div class="col-lg-3">
	            <div class="row">
					<div class="col-lg-12">
						<br><h2>Similar Artist: </h2>
					</div>
				</div>
            </div>
            -->
       	</div>
        
      </div>


<?php
	include("template/footer.php");
?>
