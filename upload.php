<?php
	include("template/header.php");
?>
    <div class="container">

      

	
      <!-- Typography
      ================================================== -->
      <!--
	  <div class="bs-docs-section">
        <div class="row">
          <div class="col-lg-12">
            <div class="page-header">
              <h1 id="type">Typography</h1>
            </div>
          </div>
        </div> -->

        <!-- Headings -->

		

        <div class="row">
			<div class="col-lg-9">
	            <div class="row">
					<div class="col-lg-12">
						<div class='page-header'>
							<h1> Upload Your Song </h1>
						</div>
					</div>
				</div>  
       			
                <?php if(isset($_SESSION['upload_error'])){ ?>
   	            <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-dismissable alert-danger">
                          <button type="button" class="close" data-dismiss="alert">×</button>
                           <?php echo $_SESSION['upload_error']; unset($_SESSION['upload_error']); ?>
                        </div>
                    </div>
                </div>
                <?php } ?>

                <form class="bs-example form-horizontal" name="form_upload_song" method="post" action="upload_appln.php" enctype="multipart/form-data"> 
                <div class="row">
                    <div class="col-lg-4">
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="fileupload-new thumbnail"> <img src="http://www.placehold.it/300x300/EFEFEF/AAAAAA&amp;text=Song+Cover+Photo" height="275px"> </div>
                            <div class="fileupload-preview fileupload-exists thumbnail"></div>
                            <div>
                                <span class="btn btn-file btn-primary">
                                    <span class="fileupload-new">Select Cover</span>
                                    <span class="fileupload-exists">Change Cover</span>
                                    <input type="file" name="song_cover" /> 
                                </span> 
                                <a href="#" class="btn fileupload-exists btn-danger" data-dismiss="fileupload">Remove</a> 
                            </div>
                            </div>
                        </div>
                
                    <div class="col-lg-8">
                        <div class="form-group">
                            <div class="col-lg-12">
                            <input type="text" class="form-control" id="inputEmail" name="song_name" placeholder="Song Name"><br>
                            <input type="text" class="form-control" id="inputEmail" name="artist_name" placeholder="Artist Name"><br>
							<div class="input-group"> <span class="input-group-addon">Contact Phone</span>
                           <input type="text" class="form-control input-medium bfh-phone" data-format="+1 (ddd) ddd-dddd" size="30" name="uphone">
                            </div><br>
                            <select class="form-control" name="promo_budget">
            					<option value="">Select promotion budget</option>
                                <?php
									$qryBudget = "select * from sf_song_budget where status = 'Active'";
									$resBudget = db_query($qryBudget);
									if(db_num_rows($resBudget)>0){
										while($rowBudget = db_fetch_object($resBudget)){
											echo "<option value='$rowBudget->budget_id'>".$rowBudget->budget."</option>";
										}
									}
								?>
							</select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12">
                            <input type="checkbox" checked name="allow_user" >&nbsp;&nbsp; Allow Users To Download This Track.
                            <br> <br>
                            <input type="file" class="form-control" style="font-size:12px; padding:none; height:auto;" name="song_path" placeholder="Select Song" accept=".mp3,.ogg,.wav,.mp4,.m4a">
                            <br> Mp3, Ogg, Wav, Mp4, M4a are Supported.
                            
                            <?php if(isset($_SESSION[sess_id])){ ?>
	                            <input type="button" onClick="chk_upload_song()" value="Upload Your Track" style="float:right;" class="btn btn-primary" name="submit_button">
                            <?php } ?> 
								
                            </div>
                        </div>
                    </div>	
                </div>
        		</form>
        	</div>
            <!--
            <div class="col-lg-3">
	            <div class="row">
					<div class="col-lg-12">
						<br><h2>Similar Artist: </h2>
					</div>
				</div>
            </div>
            -->
       	</div>
        
      </div>




<?php
	include("template/footer.php");
?>
<?php	if(!isset($_SESSION[sess_id]))	{	?>
<script>$(document).ready(function(){ $('#login_model').modal('show'); }); </script> 
<?php	} ?>
