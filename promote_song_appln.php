<?php

	include("classes/config.php");
	include("functions.php");
	
	class promote_song_appln{
		
		private $twt_status;
		private $fb_status;
		private $auto_post_mesg;
		private $song_id;
		
		function __construct(){
			
			$this->twt_status 		=  	(isset($_POST[twitter_autopost])) ? "Yes" : "No";
			$this->fb_status 		=  	(isset($_POST[fb_autopost])) ? "Yes" : "No";
			$this->auto_post_mesg	=	addslashes($_POST['auto_post_mesg']);
			$this->song_id			=	$_POST['song_id'];
			$this->submit_todb();
		}

		function get_url(){
			$qry = "select song_url from sf_song_details where song_id='".$this->song_id."' and status = 'active'";
			$res = db_query($qry);
			if(db_num_rows($res)>0)
			{
				$song_details = db_fetch_object($res);
				return $song_details->song_url;
			}
			else
			{
				return "index.php";
			}
		}
		
		function submit_todb(){
			$qry = "update `sf_song_details` set 
						`twitter_auto_post` = '$this->twt_status',
						`fb_auto_post`  	= '$this->fb_status',
						`auto_post_mesg`	= '".$this->auto_post_mesg."' 
					WHERE
						song_id = '$this->song_id' and enteredby = '".$_SESSION['sess_id']."'";
					 
			db_query($qry);
			
			$this->auto_post();			// invoking auto post
		}

		function auto_post(){
		
			if($this->fb_status == 'Yes')
			{
			
				if(!empty($_SESSION['userdetails']['id'])){																						//FACEBOOK AUTOPOST	
					$sel = db_query("select * from sf_song_details where song_id = '".$this->song_id."'");
					$res = db_fetch_object($sel);
					
					$token 		= $_SESSION[token];
					$autotitle 	= $res->song_name." at Sound Fan";
					$autouri 	= "http://www.soundfan.com/".$res->song_url;
					$autodesc 	= $this->auto_post_mesg;;
					$autopic 	= "http://www.soundfan.com/".$res->song_cover;
					$autoaction_name	= "Listen and Download";
					$autoaction_link	= "http://www.soundfan.com/".$res->song_url;
					$linkname 	= $res->song_name;

					$permission=permision($_SESSION[token]);
					if($permission==1)
					{
						facebook_Autopost($token, $autotitle, $autouri, $autodesc, $autopic, $autoaction_name, $autoaction_link, $linkname);
					}
				}
			}
			if($this->twt_status == 'Yes')
			{
				if(!empty($_SESSION['access_token'])){																							//TWITTER AUTO POST
					$sel = db_query("select * from sf_song_details where song_id = '".$this->song_id."'");
					$res = db_fetch_object($sel);
					$song_url	= "http://www.soundfan.com/".$res->song_url;
					
					$tweet_msg 	= $this->auto_post_mesg;						// tweet message
					
					if(strlen($tweet_msg)>138)
						$tweet_msg = substr($tweet_msg,0,138).'..';
						
					twitter_Autopost($tweet_msg);
				}
			}
		
		}
	}
	
	if(isset($_SESSION['sess_id'])){
	
		$obj = new promote_song_appln();
		header("location:import_contacts.php");
	}
?>