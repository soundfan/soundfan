<?php
	include("template/header.php");
?>
<style>
.thumbnail{max-height:200px !important; min-height:200px !important;}
</style>
    <div class="container">
	
      <!-- Typography
      ================================================== -->
      <!--
	  <div class="bs-docs-section">
        <div class="row">
          <div class="col-lg-12">
            <div class="page-header">
              <h1 id="type">Typography</h1>
            </div>
          </div>
        </div> -->

        <!-- Headings -->
        <?php
       
			$qry = "SELECT sf_song_details.*,
						   song_feedback.love,
						   song_feedback.hate,
						   DATE_FORMAT(date(sf_song_details.entereddate),'%m/%d/%Y') AS dt
					FROM sf_song_details
					LEFT JOIN
					  (SELECT *,
							  SUM(`hate_status`) AS hate,
							  SUM(`love_status`) AS love
					   FROM sf_song_feedback
					   GROUP BY song_id) AS song_feedback ON song_feedback.song_id = sf_song_details.song_id
					WHERE sf_song_details.enteredby = '".$_SESSION['sess_id']."' AND status = 'Active'
					ORDER BY sf_song_details.song_id DESC";
					
			$res = db_query($qry);
			
			if(db_num_rows($res)>0)
			{
								
				function get_username($user_id){
					$qry = "select * from sf_users where user_id='".$user_id."'";
					$res = db_query($qry);
					
					if(db_num_rows($res)>0)
					{
						$user = db_fetch_object($res);
						return $user->user_name;
					}
					else
					{
						echo "ADMIN";
					}
					
					return "ADMIN";
				}
			
		?>

        <div class="row">
        	<center>
                <div class="col-lg-1"></div>            
                <div class="col-lg-10">
                	<div class='page-header'>
						<h1 align="left">Dashboard</h1>
                    </div> 
                    <?php
						$qry_user = "	SELECT sf_users.*,
											   usersongcount.song_count,
											   contactimportcount.import_count,
											   song_feedback.love_count,
											   song_feedback.hate_count,
											   song_feedback.listen_count,
											   IFNULL(usercommentcount.comment_count, 0) comment_count,
											   IFNULL(userearned.amount, 0) AS amt,
											   DATE_FORMAT(date(signup_date),'%m/%d/%Y') AS dt
										FROM sf_users
										LEFT JOIN
										  (SELECT enteredby,
												  count(*) AS song_count
										   FROM sf_song_details
										   WHERE status='active'
										   GROUP BY enteredby) AS usersongcount ON usersongcount.enteredby = sf_users.user_id
										LEFT JOIN
										  (SELECT users_id,
												  count(*) AS import_count
										   FROM sf_import_contacts
										   GROUP BY users_id) AS contactimportcount ON contactimportcount.users_id = sf_users.user_id
										LEFT JOIN
										  (SELECT user_id,
										  		  song_id,
												  sum(love_status) AS love_count,
												  sum(hate_status) AS hate_count,
												  sum(listen_count) AS listen_count
										   FROM sf_song_feedback
										   ) AS song_feedback ON ( song_feedback.song_id in (select song_id from sf_song_details where enteredby = '".$_SESSION['sess_id']."'))
										LEFT JOIN
										  (SELECT user_id,
										  		  song_id,
												  count(*) AS comment_count
										   FROM sf_users_comment_song
									       GROUP BY song_id) AS usercommentcount ON (usercommentcount.song_id in (select song_id from sf_song_details where enteredby = '".$_SESSION['sess_id']."'))
										LEFT JOIN
										  (SELECT user_id, 
										  		  sum(amount) as amount 
										   FROM sf_transaction
										   GROUP BY user_id) AS userearned ON userearned.user_id = sf_users.user_id
										          
										WHERE sf_users.user_id = '".$_SESSION['sess_id']."'";
						//echo $qry_user."<br>";
						
						$res_user = db_query($qry_user);
						
						if(db_num_rows($res_user)>0):
							
							$row_user = db_fetch_object($res_user);
					?>
                        
                        <div class="panel panel-default" style="border: 1px solid #ccc;">
                          <div class='panel-body'>
                        	<div class="well icondashboard" align="center">
                            	<font color="green"><?php echo $row_user->amt; ?></font><br>
                            	Total Credit
                            </div>
                            
                        	<div class="well icondashboard" align="center">
                            	<font color="blue"><?php echo $row_user->song_count; ?></font><br>
                            	Total Tracks	
                            </div>
                            
                        	<div class="well icondashboard" align="center">
                            	<font color="orange"><?php echo $row_user->love_count; ?></font><br>
                            	Total Love Count
                            </div>
                            
                        	<div class="well icondashboard" align="center">
                            	<font color="red"><?php echo $row_user->hate_count; ?></font><br>
                            	Total Hate Count
                            </div> 
                            
                        	<div class="well icondashboard" align="center">
                            	<font color="cyan"><?php echo $row_user->comment_count; ?></font><br>
                            	Total Comments
                            </div>                                                             
                          </div>
                       	</div>                      
                    <?php
						endif;
					?>
                   	<div class="panel panel-default" style="border: 1px solid #ccc;">
                    
                    	<div class='panel-body'>
                        
                        	<?php while($row = db_fetch_object($res)){  $song_details = $row; ?>
                            <div class="panel panel-default" style="border: 1px solid #ccc;">
                    			<div class='panel-body'>
                                    <div class="col-lg-3 ">
                                        <input type="hidden" id="lovehatestatus" />
                                        <?php 
                                            if($song_details->song_cover!='' && file_exists($song_details->song_cover))
                                                echo '<img class="thumbnail" src="'.$song_details->song_cover.'">';
                                            else
                                                echo '<img class="thumbnail" src="http://www.placehold.it/300x300/EFEFEF/AAAAAA&amp;text=Song+Cover+Photo" >';
                                        ?>
                                    </div>	
                                    <div class="col-lg-9">
                                        <div class="col-lg-5" align="left">
                                            <br> <a href="http://www.soundfan.com/<?php echo $song_details->song_url ;?> "> Play this Song </a> 
                                            <br> Date Posted : 	<?php echo $song_details->dt; ?>
                                            <br> Uploaded By : 	<?php echo get_username($song_details->enteredby); ?>
                                        </div>
                                        <div class="col-lg-7">
                                        	<br /> Love Count: 	<?php echo $song_details->love; ?>
                                            <br /> Hate Count: 	<?php echo $song_details->hate; ?>
                                            <br />	<a href='https://twitter.com/share?url=http://www.soundfan.com/<?php echo $row->song_url; ?>' target='_blank'><b>Share Via Twitter </b></a> | 
                                            		<a href='https://www.facebook.com/sharer/sharer.php?u=http://www.soundfan.com/<?php echo $row->song_url; ?>' target='_blank'><b>Share Via Facebook </b></a>
                                            
                                        </div>
                                 	</div>
                        		</div>
                      		</div>
	
                            <?php } ?>
                      	</div>
                    </div>
               </div>
        	</center>
      	</div>
        
        <?php
			}
			else
			{
		?>
         <div class="row">
        	<center>
                <div class="col-lg-1"></div>            
                <div class="col-lg-10">
                	<div class='page-header'>
						<h1 align="left">My Songs</h1>
                    </div> 
                    
                   	<div class="panel panel-default" style="border: 1px solid #ccc;">
                    	<div class='panel-body'>
        				<?php	echo "Sorry You Have Not Uploaded Any Songs";	?>
        				</div>
                    </div>
              </div>
          </center>
          </div>
        <?php } ?>
   	</div>

<?php
	include("template/footer.php");
?>