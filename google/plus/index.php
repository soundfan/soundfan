<?php
/*
 * Copyright 2011 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
require_once '../src/Google_Client.php';
require_once '../src/contrib/Google_PlusService.php';
require_once '../src/contrib/Google_Oauth2Service.php';

include("../../classes/config.php");			// out database

$client = new Google_Client();
$client->setScopes("http://www.google.com/m8/feeds/");

$client->setApplicationName("soundfan-login");
// Visit https://code.google.com/apis/console to generate your
// oauth2_client_id, oauth2_client_secret, and to register your oauth2_redirect_uri.
 $client->setClientId('748769489594-ne0cagrjqkkdemde59f4oup9ob7p3ndf.apps.googleusercontent.com');
 $client->setClientSecret('e5q1czNHFU7CRjoV1a8f4vM1');
 $client->setRedirectUri('http://www.soundfan.com/google/plus/');
// $client->setDeveloperKey('insert_your_developer_key');

$plus = new Google_PlusService($client);
$oauth2 = new Google_Oauth2Service($client);
 
if (isset($_REQUEST['logout'])) {
  unset($_SESSION['access_token']);
}

if (isset($_GET['code'])) {
  $client->authenticate($_GET['code']);
  $_SESSION['access_token'] = $client->getAccessToken();
  header('Location: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
}

if (isset($_SESSION['access_token'])) {
  $client->setAccessToken($_SESSION['access_token']);
}

if ($client->getAccessToken()) {
	
  $me = $plus->people->get('me');			//for personal info
  $user = $oauth2->userinfo->get('me');		//for email
  
  /* import contacts */
  $req = new Google_HttpRequest("https://www.google.com/m8/feeds/contacts/default/full");
  $val = $client->getIo()->authenticatedRequest($req);

  // The contacts api only returns XML responses.
  $response = json_encode(simplexml_load_string($val->getResponseBody()));
  print "<pre>" . print_r(json_decode($response, true), true) . "</pre>";


  /* import contacts */
  
  
  // These fields are currently filtered through the PHP sanitize filters.
  // See http://www.php.net/manual/en/filter.filters.sanitize.php
  
  $email = filter_var($user['email'], FILTER_SANITIZE_EMAIL);
  $url = filter_var($me['url'], FILTER_VALIDATE_URL);
  $img = filter_var($me['image']['url'], FILTER_VALIDATE_URL);
  $name = filter_var($me['displayName'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
  $location = filter_var($me['placesLived'][0]['value'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
  $personMarkup = "<a rel='me' href='$url'>$name</a><div><img src='$img'></div>";

  $optParams = array('maxResults' => 100);
  $activities = $plus->activities->listActivities('me', 'public', $optParams);
  
  $activityMarkup = '';
  foreach($activities['items'] as $activity) {
  
    // These fields are currently filtered through the PHP sanitize filters.
    // See http://www.php.net/manual/en/filter.filters.sanitize.php
    $url = filter_var($activity['url'], FILTER_VALIDATE_URL);
    $title = filter_var($activity['title'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
    $content = filter_var($activity['object']['content'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
    $activityMarkup .= "<div class='activity'><a href='$url'>$title</a><div>$content</div></div>";
  
  }

  // The access token may have been updated lazily.
  $_SESSION['access_token'] = $client->getAccessToken();



/*---------------------------- Check and insert to db ----------------------------------*/

			//here the values are received in " $me "
/*			
           $sel_cnt = db_query("select count(*) cnt from sf_users where google_id = '".$me['id']."'");
		   $res_cnt = db_fetch_object($sel_cnt);
		   if($res_cnt->cnt == 0)
		   {
			   echo $res_cnt->cnt;
//			   echo "select count(*) cnt from sf_users where google_id = '".$me['id']."'";
	   		   $ins =  db_query("insert into sf_users (google_id, user_name, user_email, user_state ) values('".$me['id']."', '".$name."','".$email."','".$location."')") or die(mysql_error());
		   }
			$sel_user = db_query("select * from sf_users where google_id = '".$me['id']."'");
			$res_user = db_fetch_object($sel_user);
            
			$_SESSION[sess_id] = $res_user->user_id;
        	$_SESSION[sess_name] = $res_user->user_name;
        	$_SESSION[sess_login] = $res_user->user_email;


			echo "<script>window.location='http://soundfan.com/';</script>";
			
*/			
	
/*---------------------------- Check and insert to db ----------------------------------*/

} else {
  $authUrl = $client->createAuthUrl();
}
?>

<!--
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <link rel='stylesheet' href='style.css' />
</head>
<body>
<header><h1>Google+ Sample App</h1></header>
<div class="box">

<?php if(isset($personMarkup)): ?>
<div class="me"><?php print $personMarkup ?></div>
<?php endif ?>

<?php if(isset($activityMarkup)): ?>
<div class="activities">Your Activities: <?php print $activityMarkup ?></div>
<?php endif ?>


-->
<?php
  if(isset($authUrl)) {
    print "<script>window.location='$authUrl';</script>";
  }
  /*
   else {
   	print "<script>window.location='http://www.soundfan.com';</script>";
  }
  */
?>

<!--
</div>
</body>
</html>
-->