<?php
/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

require_once '../src/Google_Client.php';
session_start();

include("../../classes/config.php");			// including our DB

$client = new Google_Client();

$client->setApplicationName('Google Contacts PHP Sample');
$client->setScopes("http://www.google.com/m8/feeds/");
$client->setAccessType('online');
// Documentation: http://code.google.com/apis/gdata/docs/2.0/basics.html
// Visit https://code.google.com/apis/console?api=contacts to generate your
// oauth2_client_id, oauth2_client_secret, and register your oauth2_redirect_uri.
 $client->setClientId('748769489594-tcfsogcgo817cf6c1fbn8geguk3qiuk4.apps.googleusercontent.com');
 $client->setClientSecret('1jwKMIbQrkw4srM-tG7vKhK7');
 $client->setRedirectUri('http://www.soundfan.com/google/plus/simple.php');
// $client->setDeveloperKey('insert_your_developer_key');

if (isset($_GET['code'])) {
  $client->authenticate();
  $_SESSION['token'] = $client->getAccessToken();
  $redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
  header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
}

if (isset($_SESSION['token'])) {
 $client->setAccessToken($_SESSION['token']);
}

if (isset($_REQUEST['logout'])) {
  unset($_SESSION['token']);
  $client->revokeToken();
}

if ($client->getAccessToken()) {
  
/* importing contacts from google to our DB*/

	$emails=array();
	
	$qry_chk = "select * from `soundfan`.`sf_import_contacts` where users_id = '".$_SESSION['sess_id']."'";
	$res_chk = db_query($qry_chk);
	
	if(db_num_rows($res_chk) == 0)
	{
		$req = new Google_HttpRequest("https://www.google.com/m8/feeds/contacts/default/full?max-results=9999");
		$val = $client->getIo()->authenticatedRequest($req);
	}
	else
	{
		$req = new Google_HttpRequest("https://www.google.com/m8/feeds/contacts/default/full?updated-min=".date('Y-m-d')."T00:00:00");
		$val = $client->getIo()->authenticatedRequest($req);
	}

	$xml = new SimpleXMLElement($val->getResponseBody());
	$entries = $xml->children('http://www.w3.org/2005/Atom'); 
	
	foreach ($entries->entry as $entry ) { 

		$defaults = $entry->children('http://schemas.google.com/g/2005'); 
	
		$qry = "INSERT INTO  `soundfan`.`sf_import_contacts` (
					`users_id` ,
					`full_name` ,
					`email` 
				) VALUES (
					'".$_SESSION['sess_id']."',  
					'".$entry->title."',  
					'".$defaults->email->attributes()->address."'
				);";
	
		db_query($qry);
	}

	header("location:../../contact_mail_list.php");
 
		
/* importing contacts from google to our DB*/

  // The access token may have been updated lazily.
  $_SESSION['token'] = $client->getAccessToken();
} else {
  $auth = $client->createAuthUrl();
}

if (isset($auth) && isset($_SESSION['sess_id'])) 
{
	/* save redirecting url   */
	$_SESSION['redirect_url'] =  $_SERVER['HTTP_REFERER'];
	header("location:".$auth);
}
else
	print_r($_SESSION);
