<?php
	include("template/header.php");
	
	if(!isset($_SESSION['sess_id']))
	{
		header("location:index.php");
		exit;
	}
?>
    <div class="container">
	
      <!--     
      <div class="page-header" id="banner">
        <div class="row">
          <div class="col-lg-6">
   			 <h1>Flatly</h1>
            <p class="lead">Flat and modern</p> 
          </div>

        </div>
      </div>-->

	
      <!-- Typography
      ================================================== -->
      <!--
	  <div class="bs-docs-section">
        <div class="row">
          <div class="col-lg-12">
            <div class="page-header">
              <h1 id="type">Typography</h1>
            </div>
          </div>
        </div> -->

        <!-- Headings -->
	<div class="row">
    	<div class="col-lg-12">
        	<div class='page-header'>
            	<h1 align="left">Settings</h1>
        	</div> 
            
            <?php
				$qry = "select * from sf_users where user_id='".$_SESSION['sess_id']."'";
				$res = db_query($qry);
				
				if(db_num_rows($res)>0)
					$row = db_fetch_object($res);
				
			?>
    		
            <form action="settings_appln.php" method="post" name="user_settings">
             <div class="row">
                <div class="col-lg-12">
                <legend>Auto Post Settings</legend>
                <table cellpadding="7">
                <tbody><tr><td>Twitter Auto Post</td>
                <td>
                    <div class="make-switch switch-small" data-on="success" data-off="danger">
                     <input type="checkbox" name="twitter_autopost" <?php echo ($row->twt_autopost == "Yes") ? "checked" : ""; ?> value="" >
                    </div>
                </td>
                </tr>
                <tr><td>Facebook Auto Post</td>
                <td>
                    <div class="make-switch switch-small" data-on="success" data-off="danger">
                     <input type="checkbox" name="fb_autopost" <?php echo ($row->fb_autopost == "Yes") ? "checked" : ""; ?> value="">
                    </div>
                </td>
                </tr>
                <tr><td>Daily Mail</td>
                <td>
                    <div class="make-switch switch-small" data-on="success" data-off="danger">
                     <input type="checkbox" name="daily_mail" <?php echo ($row->daily_mail == "Yes") ? "checked" : ""; ?> value="">
                    </div>
                </td>
                </tr>
                <tr><td>Daily Autopost</td>
                <td>
                    <div class="make-switch switch-small" data-on="success" data-off="danger">
                     <input type="checkbox" name="daily_autopost" <?php echo ($row->daily_autopost == "Yes") ? "checked" : ""; ?> value="">
                    </div>
                </td>
                </tr>
                </tbody></table>
                <br />
                </div>
            </div>

            <div class="row">
                <div align="center">
                	<hr />
                    <input name="signup_hid" type="hidden" value="Yes" />
                    <button type="submit" class="btn btn-success" id="signup_button" onclick=""><i class="icon-white icon-ok"></i> Save changes</button>
                </div>
            </div>
			</form>
        </div>
	</div>
		
    </div>

<?php
	include("template/footer.php");
?>