<?php
	$song_id = "";

	if(filter_has_var(INPUT_GET,"song_id"))
	{
		$song_id = $_GET['song_id'];
	}
	else
	{
		header('location:index.php');
		exit;
	}
	include("template/header.php");	
?>
    <div class="container">

      <!-- Typography
      ================================================== -->
      <!--
	  <div class="bs-docs-section">
        <div class="row">
          <div class="col-lg-12">
            <div class="page-header">
              <h1 id="type">Typography</h1>
            </div>
          </div>
        </div> -->

        <!-- Headings -->
	<?php
       $qry = "select *,DATE_FORMAT(date(entereddate),'%m/%d/%Y') as dt from sf_song_details where song_id='".$song_id."' and status = 'inactive'";
			$res = db_query($qry);
			
			if(db_num_rows($res)>0)
			{
				$song_details = db_fetch_object($res);
				
				function get_username($user_id){
					
					
					$qry = "select * from sf_users where user_id='".$user_id."'";
					$res = db_query($qry);
					
					if(db_num_rows($res)>0)
					{
						$user = db_fetch_object($res);
						if(!empty($user->twitter_id))
							return "<a href='https://twitter.com/".$user->screen_name."'>@".$user->screen_name."</a>";
						else if(!empty($user->facebook_id))
							return "<a href='https://facebook.com/profile.php?id=".$user->facebook_id."'>".$user->user_name."</a>";
						else
							return $user->user_name;
					}
					else
					{
						echo "ADMIN";
					}
				}
			
		?>

        <div class="row">
        	<center>
                <div class="col-lg-1"></div>            
                <div class="col-lg-10">
                	<div class='page-header'>
						<h1 align="left">Share Your Song</h1>
                    </div> 
                    
                   	<div class="panel panel-default" style="border: 1px solid #ccc;">
                    	<div class='panel-body'>
                            <div class="col-lg-2"> 
                            	<br>
                                <?php 
                                    if($song_details->song_cover!='' && file_exists($song_details->song_cover))
                                        echo '<img class="thumbnail" src="'.$song_details->song_cover.'">';
                                    else
                                        echo '<img class="thumbnail" src="http://www.placehold.it/300x300/EFEFEF/AAAAAA&amp;text=Song+Cover+Photo" >';
                                ?>
                            </div>	
                            <div class="col-lg-10">
                           		<div class="col-lg-5" align="left">
                            	<br> Date Posted : <?php echo $song_details->dt; ?>
                                <br> Uploaded By : <?php echo get_username($song_details->enteredby); ?>
                                <br />
                                <?php
									$qry_user = "select * from sf_song_details where enteredby = '".$_SESSION[sess_id]."' and song_id='". $song_details->song_id."'";
									$res_user = db_query($qry_user);
									if(db_num_rows($res_user)>0){
								?>
                                	<a href="stat.php?song_id=<?php echo $song_details->song_id; ?>"> View My Stats </a>
                                <?php } ?>
                                
                                <!-- AddThis Smart Layers BEGIN -->
                                <!-- Go to http://www.addthis.com/get/smart-layers to customize -->
                                <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=mrlinker"></script>
                                <script type="text/javascript">
                                  addthis.layers({
                                    'theme' : 'transparent',
                                    'share' : {
                                      'position' : 'left',
                                      'numPreferredServices' : 5
                                    }   
                                  });
                                </script>
                                <!-- AddThis Smart Layers END -->
                                
                                <!-- AddThis Button BEGIN 
                                <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
                                    <a class="addthis_button_preferred_1"></a>
                                    <a class="addthis_button_preferred_2"></a>
                                    <a class="addthis_button_preferred_3"></a>
                                    <a class="addthis_button_preferred_4"></a>
                                    <a class="addthis_button_compact"></a>
                                    <a class="addthis_counter addthis_bubble_style"></a>
                                </div>
                                <script type="text/javascript">var addthis_config = {"data_track_addressbar": true};</script>
                                <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=mrlinker"></script>
                                AddThis Button END -->
                            </div>
                           		<div class="col-lg-7">
                            <br />
                            	<!-- Audio Player Starts-->
                                
                            	<link rel="stylesheet" href="css/audioplayer.css" />
								<?php
									if($song_details->song_path!='' && file_exists($song_details->song_path))
                                	{
										$load_audio = "no";
										$ext 	= pathinfo($song_details->song_path, PATHINFO_EXTENSION);
										if($ext == "m4a")
										{
											$load_audio = "yes";
										}
										$useragent = $_SERVER['HTTP_USER_AGENT'];
										$browser_code 	= '/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i';
										$browser_ver	= '/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i';
										
										
										if(preg_match($browser_code,$useragent)||preg_match($browser_ver,substr($useragent,0,4)))
											echo '<audio id="music_player_upload"  preload="auto" controls >'; // autoplay for auto playing 
										else
											echo '<audio id="music_player_upload"  preload="auto" controls autoplay >'; // autoplay for auto playing 
									
										echo '<source src="'.$song_details->song_path.'">';
										echo '</audio>';
										
										echo "<input type='hidden' value='".$song_id."' id='songid'>";
									}
									else
									{
										echo "Error";
									}
								?>
                                <!-- Audio Player Ends -->
                               
                            </div>
                        	</div>
                      	</div>
                    </div>
               </div>
        	</center>
      	</div>
        
        <div class="row">
	        <div class="col-lg-1"></div>
            <div class="col-lg-10">
            	<input type="text" class="form-control" id="song_url" onclick="this.focus();this.select()" value="http://www.soundfan.com/<?php echo $song_details->song_url;?>" name="song_url" ><br>
            </div>
       	</div>
        
        <div class="row">
            <div class="col-lg-1"></div>
            <div class="col-lg-5">
                <div class="well">
                    <h3 align="center" style="color:#4DB848"><a href="#" onclick="window.open('https://twitter.com/share?url=http://www.soundfan.com/<?php echo $song_details->song_url;?>','_blank','top=200,left=415,width=500,height=300,resizable=no', true)" target="_blank">Tweet</a></h3>
                    <p align="center">Share Via Twitter</p>
                </div> 
            </div>

            <div class="col-lg-5">                        
                <div class="well">
                    <h3 align="center" style="color:#ff4056">
                        <a href="#" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=http://www.soundfan.com/<?php echo $song_details->song_url;?>','_blank','top=200,left=415,width=500,height=300,resizable=no', true)" target="_blank">Facebook</a>
                    </h3>
                    <p align="center">Share Via Facebook</p>
                </div>
            </div>
        </div>
        <div id="row">
        	<div class="col-lg-10 col-lg-offset-1">
            	<form action="upload2_appln.php" method="post">
             		<input type="hidden" value="<?php echo $song_id; ?>" name="song_id" />
             		<input type="submit" value="Complete Upload" style="float:right;" class="btn btn-primary" name="submit_button" />
         		</form>
            </div>
        </div>
        
        
        <?php
			}
			else
			{
				echo "<script>window.location='index.php'; </script>";
			}
		?>
   	</div>
        	</div>
            <!--
            <div class="col-lg-3">
	            <div class="row">
					<div class="col-lg-12">
						<br><h2>Similar Artist: </h2>
					</div>
				</div>
            </div>
            -->
       	</div>
        
      </div>


<?php
	include("template/footer.php");
?>
