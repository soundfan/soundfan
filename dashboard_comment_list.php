<?php
	session_start();
	if(!isset($_SESSION['sess_id'])){
		header("location:index.php");
		exit;
	}

	include("template/header.php");
?>
<style>
.thumbnail{max-height:200px !important; min-height:200px !important;}
</style>
    <div class="container">
	
      <!-- Typography
      ================================================== -->
      <!--
	  <div class="bs-docs-section">
        <div class="row">
          <div class="col-lg-12">
            <div class="page-header">
              <h1 id="type">Typography</h1>
            </div>
          </div>
        </div> -->

        <!-- Headings -->
        <?php
      						
				function get_username($user_id){
					$qry = "select * from sf_users where user_id='".$user_id."'";
					$res = db_query($qry);
					
					if(db_num_rows($res)>0)
					{
						$user = db_fetch_object($res);
						return $user->user_name;
					}
					else
					{
						echo "ADMIN";
					}
					
					return "ADMIN";
				}
			
		?>

        <div class="row">
        	<center>
                <div class="col-lg-1"></div>            
                <div class="col-lg-10">
                	<div class='page-header'>
						<h1 align="left"> <a href="dashboard_artist.php"> <img src="images/back.png" border="0" class="" style="vertical-align:middle">  </a> | Comment List</h1>
                    </div> 
					
                    <?php
						function get_comments($song_id){
							$qry = "select sf_users_comment_song.*,date_format(dated,'%m/%d/%Y') dt, sf_users.user_name from sf_users_comment_song left join sf_users on sf_users.user_id = sf_users_comment_song.user_id where sf_users_comment_song.song_id = '".$song_id."' ";
							$res = db_query($qry);
							
							if(db_num_rows($res)>0):
					?>
							<table class="table table-responsive" cellpadding='10' cellspacing='10' width="100%">
                            	<tr>
                                	<th>Name</th>
                                    <th>Comment</th>
                                    <th>Dated</th>
                                </tr>
					<?php			
								while($row = db_fetch_object($res)):
					?>
                    			<tr>
                                	<td> <?php echo $row->user_name; ?> </td>
                                    <td> <?php echo $row->comment; ?>	</td>
                                    <td> <?php echo $row->dt; ?>		</td>
                                </tr>
                    <?php				
								endwhile;
					?>
							</table>
                    <?php
							endif;
						}
					
						$qry = "select sf_song_details.* from sf_song_details
								LEFT JOIN
								(SELECT *,
									  SUM(`hate_status`) AS hate,
									  SUM(`love_status`) AS love
								FROM sf_song_feedback
								GROUP BY song_id) AS song_feedback ON song_feedback.song_id = sf_song_details.song_id where sf_song_details.song_id in (select distinct song_id from sf_users_comment_song) and enteredby = '".$_SESSION['sess_id']."' and sf_song_details.status = 'Active'";
						$res = db_query($qry);
					?>
                    
                   	<div class="panel panel-default" style="border: 1px solid #ccc;">
                    
                    	<div class='panel-body'>
                        
                        	<?php while($row = db_fetch_object($res)){  $song_details = $row; ?>
                            <div class="panel panel-default" style="border: 1px solid #ccc;">
                    			<div class='panel-body'>
                                    <div class="col-lg-3 ">
                                        <input type="hidden" id="lovehatestatus" />
                                        <a href="http://www.soundfan.com/<?php echo $song_details->song_url ;?> ">
                                        <?php 
                                            if($song_details->song_cover!='' && file_exists($song_details->song_cover))
                                                echo '<img class="thumbnail" src="'.$song_details->song_cover.'">';
                                            else
                                                echo '<img class="thumbnail" src="http://www.placehold.it/300x300/EFEFEF/AAAAAA&amp;text=Song+Cover+Photo" >';
                                        ?>
                                        </a>
                                    </div>	
                                    <div class="col-lg-9">
                                        <div class="col-lg-5" align="left">
                                            <br> <a href="http://www.soundfan.com/<?php echo $song_details->song_url ;?> "> Play this Song </a> 
                                            <br> Date Posted : 	<?php echo $song_details->dt; ?>
                                            <br> Uploaded By : 	<?php echo get_username($song_details->enteredby); ?>
                                        </div>
                                        <div class="col-lg-7">
                                        	<br /> Love Count: 	<?php echo $song_details->love; ?>
                                            <br /> Hate Count: 	<?php echo $song_details->hate; ?>
                                            <br />	<a href='https://twitter.com/share?url=http://www.soundfan.com/<?php echo $row->song_url; ?>' target='_blank'><b>Share Via Twitter </b></a> | 
                                            		<a href='https://www.facebook.com/sharer/sharer.php?u=http://www.soundfan.com/<?php echo $row->song_url; ?>' target='_blank'><b>Share Via Facebook </b></a>
                                            
                                        </div>
                                        <div class="clearfix"><br><br></div>
			                            <div class="col-lg-12">
                                    		<?php get_comments($row->song_id); ?>
	                                    </div>
                                 	</div>

                        		</div>
                      		</div>
	
                            <?php } ?>
                      	</div>
                    </div>
               </div>
        	</center>
      	</div>
   	</div>

<?php
	include("template/footer.php");
?>