<?php
	$song_id = "";
	//	print_r($_GET);
	if(filter_has_var(INPUT_GET,"song_id"))
	{
		$song_id = $_GET['song_id'];
	}
	else if(filter_has_var(INPUT_GET,"sn"))
	{
		include 'classes/config.php';
		$qry_getsong = "select * from sf_song_details where song_url='".$_GET['sn'].".html'";
		$res_getsong = db_query($qry_getsong);
		if(db_num_rows($res_getsong) > 0){
			$row_getsong = db_fetch_object($res_getsong);
			$song_id = $row_getsong->song_id;
		}
		else{
			header('location:index.php');
			exit;
		}
	}
	else
	{
		header('location:index.php');
		exit;
	}
	include("template/header.php");
?>
    <div class="container">
	
      <!-- Typography
      ================================================== -->
      <!--
	  <div class="bs-docs-section">
        <div class="row">
          <div class="col-lg-12">
            <div class="page-header">
              <h1 id="type">Typography</h1>
            </div>
          </div>
        </div> -->

        <!-- Headings -->
        <?php
       
	   		// deleting history starts....
			clear_history();
			function clear_history(){
				$qry = "delete from sf_temp_song_listen_stat where date(listen_date) < CURDATE();";
				db_query($qry);
			}
			// deleting history ends....
	   
	   		$qry = "select *,DATE_FORMAT(date(entereddate),'%m/%d/%Y') as dt from sf_song_details where song_id='".$song_id."' and status = 'active' limit 1;";
			$res = db_query($qry);
			
			if(db_num_rows($res)>0)
			{
				$song_details = db_fetch_object($res);
				
				function get_username($user_id){
					
					
					$qry = "select * from sf_users where user_id='".$user_id."'";
					$res = db_query($qry);
					
					if(db_num_rows($res)>0)
					{
						$user = db_fetch_object($res);
						if(!empty($user->twitter_id))
							return "<a href='https://twitter.com/".$user->screen_name."'>@".$user->screen_name."</a>";
						else if(!empty($user->facebook_id))
							return "<a href='https://facebook.com/profile.php?id=".$user->facebook_id."'>".$user->user_name."</a>";
						else
							return $user->user_name;
					}
					else
					{
						echo "ADMIN";
					}
				}
			
		?>

        <div class="row">
        	<center>
                <div class="col-lg-1"></div>            
                <div class="col-lg-10">
                	
                	
                	<iframe id="sc-widget" width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https://soundcloud.com/octobersveryown/drake-back-to-back-freestyle&show_artwork=true"></iframe>
					<script src="https://w.soundcloud.com/player/api.js" type="text/javascript"></script>
					<script type="text/javascript">
					  (function(){
					    var widgetIframe = document.getElementById('sc-widget'),
					        widget       = SC.Widget(widgetIframe),
					        newSoundUrl = 'https://soundcloud.com/octobersveryown/drake-back-to-back-freestyle';
					
					    widget.bind(SC.Widget.Events.READY, function() {
					      // load new widget
					      widget.bind(SC.Widget.Events.FINISH, function() {
					        widget.load(newSoundUrl, {
					          show_artwork: false
					        });
					      });
					    });
					
					  }());
					</script>
                	
                	
                	
               </div>
        	</center>
      	</div>
        
        <?php
			}
			else
			{
				echo "<script>window.location = 'index.php'; </script>";
			}
		?>
   	</div>
    
    <?php
		
			if(isset($_SESSION['sess_id']))
			{
				if(!isset($_SESSION['show_popup'])){
					$qry_chk_listen = "select * from sf_song_feedback where song_id='".$song_id."' and user_id='".$_SESSION['sess_id']."' and listen_count=0";
					$res_chk_listen = db_query($qry_chk_listen);
					if(!(db_num_rows($res_chk_listen) > 0)){
						show_model();
					}
					else{
						alreadyrated_model();
					}
				}
				else{
					$qry_chk_listen = "update sf_temp_song_listen_stat set user_id='".$_SESSION['sess_id']."' where song_id = '".$song_id."' and session_id='".session_id()."'";
					$res_chk_listen = db_query($qry_chk_listen);
					
					$qry_chk_listen = "update sf_song_feedback set user_id='".$_SESSION['sess_id']."' where song_id = '".$song_id."' and session_id='".session_id()."'";
					$res_chk_listen = db_query($qry_chk_listen);			
				}

			}
			else
			{
				$qry_chk_listen = "select * from sf_temp_song_listen_stat where song_id='".$song_id."' and session_id='".session_id()."'";
				$res_chk_listen = db_query($qry_chk_listen);
				if(!(db_num_rows($res_chk_listen) > 0)){
					show_model();
				}
				else{
					alreadyrated_model();
				}
			}
		
		function get_next_song(){
			
			$qry = "select song_url from sf_song_details where status='active' and song_id not in( select song_id from sf_temp_song_listen_stat where session_id='".session_id()."') order by rand() limit 1";
			$res = db_query($qry);
			if(db_num_rows($res) > 0){
				$row = db_fetch_object($res);
				return $row->song_url;
			}else{
				$qry = "select song_url from sf_song_details where status='active' order by rand() limit 1";
				$res = db_query($qry);
				$row = db_fetch_object($res);
				return $row->song_url;
			}
		}
		
		function show_model(){
			global $song_id;
	?>
    
    <!--for modal-->
	<!-- Modal -->
    <form name="feedback_form" method="post">
	  	<div class="modal fade" id="feedbackmodel" tabindex="-1" data-backdrop="false" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      		<div class=" modal-backdrop" style="opacity:0.5;"></div>
			<div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                 
                 <h4 class="modal-title">Leave feedback for the Artist</h4>
                </div>
                <div class="modal-body" style="word-wrap: break-word;">
                    <center>
                        This artist would love to have some feedback on their song. Let us know if you "Love it" or "Hate it".
                        <br /> <br />                  
                        <div class="row">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-3">
                                <button type="button" class="btn btn-success"  data-dismiss="modal" id="love_song" value="<?php echo $song_id; ?>" aria-hidden="true">Love It</button> 
                            </div>
                            <div class="col-lg-2"></div>
                            <div class="col-lg-3">                        
                                <button type="button" class="btn btn-danger"  data-dismiss="modal" id="hate_song" value="<?php echo $song_id; ?>" aria-hidden="true">Hate It</button>
                            </div>
                        </div>
                    </center>             
                </div>
                <div class="modal-footer">
                <!--
                    <div class="row">
                    	<span id="buy_download">
                            <div class="col-lg-5">
                           		<input type="checkbox" id="buy_it" /> I Would Buy This Song
                            </div>
                            <div class="col-lg-1"></div>
                            <div class="col-lg-5">
                            	<input type="checkbox" id="download_it" /> I Would Download This Song	                
                            </div>			
                        </span>
                    <textarea class="form-control" id="comment"></textarea>
                    <br />
                    <button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Submit</button>
                -->
                </div>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
	  </div>
    </div>
    </form><!-- /.modal -->
	<!--for modal-->
	
    <?php	}	?>
 
 	<?php 
		function alreadyrated_model(){
	?>
 
 	<!--already rated_model-->
	<!-- Modal -->
      	<div class="modal fade" id="feedbackmodel" tabindex="-1" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      		<div class=" modal-backdrop" style="opacity:0.5;"></div>
			<div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
	             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Leave feedback for the Artist</h4>
                </div>
                <div class="modal-body" style="word-wrap: break-word;">
                    <center>
						
                        You have already rated this song
                        
                        <br /> <br />                  
  						<button type="button" class="btn btn-success" id="skip_song" data-dismiss="modal" aria-hidden="true">Skip >></button> 
                    </center>             
                </div>
                <div class="modal-footer">
                </div>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
	  </div>
   	</div><!-- /.modal -->
	<!--already rated_model-->
 	<?php 	} ?>
    
    <!--love rate modal-->
	<!-- Modal -->
    <div class="modal fade" id="loveratemodel" tabindex="-1" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class=" modal-backdrop" style="opacity:0.5;"></div>
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
             <h4 class="modal-title">Leave your feedback</h4>
            </div>
            <div class="modal-body" style="word-wrap: break-word;">
                <center>
                <?php if(!isset($_SESSION[sess_id])){ ?>
                Please Login : <a href="#" onclick="window.open('FacebookLogin.php?&location=upload.php&show_popup=yes','_blank','top=200,left=415,width=500,height=300,resizable=no', true)"><img src="images/fb.png"></a><br />
                <?php } ?>
                    <?php
                        $qry = "SELECT *, SUM(  `hate_status` ) AS hate, SUM(  `love_status` ) AS love FROM sf_song_feedback WHERE song_id = '".$song_id."' ";

                        $res = db_query($qry) or die(mysql_error());
                        
                        if(db_num_rows($res)>0)
                        {
                            $row_songdetails = db_fetch_object($res);
                            
                            $hate  	= ( $row_songdetails->hate == NULL )? 0 : $row_songdetails->hate;
                            $love  	= ( $row_songdetails->love == NULL )? 0 : $row_songdetails->love;
                        }
                    ?>
                   
                    <div id="update_count">
                    <?php	echo "Thanks for your rating. Start earning money for each song you leave feedback by logging in with Facebook. You agreed with $love other people and loved this song";	?>
                    
                    <br /> <br />                  
                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-5">
                            <div class="well">
                                <h1 align="center" style="color:#4DB848"><strong><?php echo $love; ?></strong></h1>
                                <p align="center">Love Count</p>
                            </div> 
                        </div>

                        <div class="col-lg-5">                        
                            <div class="well">
                                <h1 align="center" style="color:#ff4056"><strong><?php echo $hate; ?></strong></h1>
                                <p align="center">Hate Count</p>
                            </div>
                        </div>
                    </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-10" id="inputUserComment_div" align="right">
                            <textarea class="form-control" id="inputUserComment" placeholder="Leave artist a feedback.." name="comment"></textarea>
                            <input type="hidden" value="<?php echo $song_id; ?>" name="songid" id="song_id"  />
                            <p style="float:left">You must leave a comment to earn $$$</p>
                            <button type="button" class="btn btn-success btn-sm" id="submitcomment" aria-hidden="true">Send</button> 
                            <br /><br />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-5">
                            <div class="well">
                                <h3 align="center" style="color:#4DB848"><a href="#" onclick="window.open('https://twitter.com/share','_blank','top=200,left=415,width=500,height=300,resizable=no', true)">Tweet</a></h3>
                                <p align="center">Share Via Twitter</p>
                            </div> 
                        </div>

                        <div class="col-lg-5">                        
                            <div class="well">
                                <h3 align="center" style="color:#ff4056">
                                    <a href="#" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?php echo (($_SERVER['HTTPS'] == 'on') ? 'https' : 'http').'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>','_blank','top=200,left=415,width=500,height=300,resizable=no', true)">Facebook</a>
                                </h3>
                                <p align="center">Share Via Facebook</p>
                            </div>
                        </div>
                    </div>
                </center>             
            </div>
            <div class="modal-footer">
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	<!--love rate modal-->
 

<?php
	include("template/footer.php");
?>
<?php	//if(isset($_SESSION['show_popup']))	{	?>
	<script>$(document).ready(function(){ $('#loveratemodel').modal('show'); }); </script> 
<?php	//unset($_SESSION['show_popup']); } ?>