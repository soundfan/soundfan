<?php
/*--------------------------------------------
  Author : Sooraj 			
----------------------------------------------
*/
	session_start();
	
	// TO REDIRECT TO PREVIOUS URL 
	if(!isset($_SESSION['HTTP_REFERER'])){
		$_SESSION['HTTP_REFERER']	=	$_SERVER['HTTP_REFERER'];
	}
	// TO POP UP THE LOVE HATE COUNT	
	if(filter_has_var(INPUT_GET,"show_popup")){
			$_SESSION['show_popup']	=	"yes";
	}



include("template/header.php");
if (isset($_GET['error_reason'])) {
    echo"<script>self.close()</script>";
}

/**
 * Copyright 2011 Facebook, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
require 'facebook/facebook.php';

// Create our Application instance (replace this with your appId and secret).
$facebook = new Facebook(array(						
            'appId' => '1436998323189487',
            'secret' => 'be9908c0aef3b0c67d4ac0d6d62c2fbd',
        ));


// Get User ID
$user = $facebook->getUser();
$access_token = $facebook->getAccessToken();
// We may or may not have this data based on whether the user is logged in.
//
// If we have a $user id here, it means we know the user is logged into
// Facebook, but we don't know if the access token is valid. An access
// token is invalid if the user logged out of Facebook.

if ($user) {
    try {
        // Proceed knowing you have a logged in user who's authenticated.
        $user_profile = $facebook->api('/me');
		$permissions = $facebook->api("/me/permissions");
		$permission="Yes";
/*if (!array_key_exists('publish_stream', $permissions['data'][0])) 
$permission="No";	
if (!array_key_exists('manage_pages', $permissions['data'][0])) 
$permission="No";	*/


    } catch (FacebookApiException $e) {
        error_log($e);
        $user = null;
    }
}

// Login or logout url will be needed depending on current user state.
if ($user) {
    $logoutUrl = $facebook->getLogoutUrl(array(
        'next' => $baseURL.'logout.php',
            ));
} else {
    $loginUrl = $facebook->getLoginUrl(array(
        'display' => 'popup',
		'scope' => 'email,publish_stream',				//,manage_pages,publish_stream,read_insights
            ));
}
 

?>
<!doctype html>
<html xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>
   
    </head>
    <body>   

        <?php
        if ($user):
            session_start();
            $_SESSION['id'] = $user;
            $_SESSION['loguturl'] = $logoutUrl;
            $_SESSION['userdetails'] = $user_profile;
            $_SESSION['token'] = $access_token;
			
            /* ----------------- VALUES ------------------------ */
           	$fbid = $_SESSION['userdetails']['id'];
         	
			$user_first_name = $_SESSION['userdetails']['first_name'];
			$user_last_name = $_SESSION['userdetails']['last_name'];
			$user_email = $_SESSION['userdetails']['email'];
			$user_birthday = $_SESSION['userdetails']['birthday'];
			$user_gender = $_SESSION['userdetails']['gender'];
			$user_hometown_name = $_SESSION['userdetails']['hometown']['name'];
			$user_link = $_SESSION['userdetails']['link'];
			$username = $_SESSION['userdetails']['name'];
			$location = $_SESSION['userdetails']['location']['name'];
			$uphone = $_SESSION['userdetails']['mobile_phone'];
            /* -------------------------------------------------- */

										 
           $sel_cnt = db_query("select count(*) cnt from sf_users where facebook_id = '$fbid'");
		   $res_cnt = db_fetch_object($sel_cnt);
		   if($res_cnt->cnt==0)
		   {
		   		$ins =  db_query("insert into sf_users (facebook_id, user_name, user_email, user_city, user_state )									   
values('$fbid', '$username','$user_email','$location','$user_hometown_name')");
		   }
			$sel_user = db_query("select * from sf_users where facebook_id = '$fbid'");
			$res_user = db_fetch_object($sel_user);
            
			$_SESSION[sess_id] = $res_user->user_id;
        	$_SESSION[sess_name] = $res_user->user_name;
        	$_SESSION[sess_login] = $res_user->user_email;
//---------------------------------------------------------------------------------------------------
    if ($_GET[frame] == "self") {
        echo "<script>
window.open('index.php','_self');
//self.close();
</script>";
    } else {
        echo"<script>
//window.opener.location.reload(true);
window.opener.location.href='".$_SESSION['HTTP_REFERER']."';
self.close();
</script>";
    }

else:
    ?>
            <div>
                <script>top.location.href="<?php echo $loginUrl; ?>"</script>
            </div>
<?php endif ?>
<?php
include("template/footer.php");
?>