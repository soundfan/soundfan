<?php
	session_start();
	if(!isset($_SESSION['sess_id'])){
		header("location:index.php");
		exit;
	}

	include("template/header.php");
?>
<style>
.thumbnail{max-height:200px !important; min-height:200px !important;}
</style>
    <div class="container">
	
      <!-- Typography
      ================================================== -->
      <!--
	  <div class="bs-docs-section">
        <div class="row">
          <div class="col-lg-12">
            <div class="page-header">
              <h1 id="type">Typography</h1>
            </div>
          </div>
        </div> -->

        <!-- Headings -->
        <?php
       
			$qry = "SELECT sf_song_details.*,
						   song_feedback.love,
						   song_feedback.hate,
						   DATE_FORMAT(date(sf_song_details.entereddate),'%m/%d/%Y') AS dt
					FROM sf_song_details
					LEFT JOIN
					  (SELECT *,
							  SUM(`hate_status`) AS hate,
							  SUM(`love_status`) AS love
					   FROM sf_song_feedback
					   GROUP BY song_id) AS song_feedback ON song_feedback.song_id = sf_song_details.song_id
					WHERE sf_song_details.enteredby = '".$_SESSION['sess_id']."' AND sf_song_details.status = 'Active'
					ORDER BY sf_song_details.song_id DESC";
					
			$res = db_query($qry);
			
			if(db_num_rows($res)>0)
			{
								
				function get_username($user_id){
					$qry = "select * from sf_users where user_id='".$user_id."'";
					$res = db_query($qry);
					
					if(db_num_rows($res)>0)
					{
						$user = db_fetch_object($res);
						return $user->user_name;
					}
					else
					{
						echo "ADMIN";
					}
					
					return "ADMIN";
				}
      						
				function get_comments($song_id){
					
					$qry = "select sf_users_comment_song.*,date_format(dated,'%m/%d/%Y') dt, sf_users.user_name from sf_users_comment_song left join sf_users on sf_users.user_id = sf_users_comment_song.user_id where sf_users_comment_song.song_id = '".$song_id."' order by id desc ";
					$res = db_query($qry);
					
					if(db_num_rows($res)>0):
			?>
					<table class="table table-responsive" cellpadding='10' cellspacing='10' width="100%">
						<tr>
							<th>Name</th>
							<th>Comment</th>
							<th>Dated</th>
						</tr>
			<?php			
						while($row = db_fetch_object($res)):
			?>
						<tr>
							<td> <?php echo $row->user_name; ?> </td>
							<td> <?php echo $row->comment; ?>	</td>
							<td> <?php echo $row->dt; ?>		</td>
						</tr>
			<?php				
						endwhile;
			?>
					</table>
			<?php
					endif;
				}
			?>

        <div class="row">
        	<center>
                <div class="col-lg-1"></div>            
                <div class="col-lg-10">
                	<div class='page-header'>
						<h1 align="left">Dashboard</h1>
                    </div> 
                    <?php
						$qry_user = "	SELECT sf_users.*,
											   IFNULL(usersongcount.song_count, 0) song_count,
											   IFNULL(contactimportcount.import_count, 0) import_count,
											   IFNULL(song_feedback.love_count, 0) love_count,
											   IFNULL(song_feedback.hate_count, 0) hate_count,
											   IFNULL(song_feedback.listen_count, 0) listen_count,
											   IFNULL(usercommentcount.comment_count, 0) comment_count,
											   IFNULL(userearned.amount, 0) AS earnamt,
											   IFNULL(userdeduted.amount, 0) AS deductamt,											   
											   DATE_FORMAT(date(signup_date),'%m/%d/%Y') AS dt
										FROM sf_users
										LEFT JOIN
										  (SELECT enteredby,
												  count(*) AS song_count
										   FROM sf_song_details
										   WHERE status='active'
										   GROUP BY enteredby) AS usersongcount ON usersongcount.enteredby = sf_users.user_id
										LEFT JOIN
										  (SELECT users_id,
												  count(*) AS import_count
										   FROM sf_import_contacts
										   GROUP BY users_id) AS contactimportcount ON contactimportcount.users_id = sf_users.user_id
										LEFT JOIN
										  (SELECT user_id,
										  		  song_id,
												  sum(love_status) AS love_count,
												  sum(hate_status) AS hate_count,
												  sum(listen_count) AS listen_count
										   FROM sf_song_feedback
                                           where song_id in (select song_id from sf_song_details where enteredby = '".$_SESSION['sess_id']."')
										   ) AS song_feedback ON ( true )
										LEFT JOIN
										  (SELECT user_id,
										  		  song_id,
												  count(*) AS comment_count
										   FROM sf_users_comment_song
										   where song_id in (select song_id from sf_song_details where enteredby = '".$_SESSION['sess_id']."')) AS usercommentcount ON (true)
										LEFT JOIN
										  (SELECT user_id, 
										  		  sum(amount) as amount 
										   FROM sf_transaction where flag = 'earn rate'
										   GROUP BY user_id) AS userearned ON userearned.user_id = sf_users.user_id
										LEFT JOIN
										  (SELECT user_id, 
										  		  sum(amount) as amount 
										   FROM sf_transaction where (flag = 'deduct rate' or flag = 'deduct comment' or flag = 'initial')
										   GROUP BY user_id) AS userdeduted ON userdeduted.user_id = sf_users.user_id
										   
										WHERE sf_users.user_id = '".$_SESSION['sess_id']."'";
						//echo $qry_user."<br>";
						
						$res_user = db_query($qry_user);
						
						if(db_num_rows($res_user)>0):
							
							$row_user = db_fetch_object($res_user);
					?>
                        
                        <div class="panel panel-default" style="border: 1px solid #ccc;">
                          <div class='panel-body'>
                        	<!--
                            <div class="well icondashboard" align="center">
                            	<font color="green"> $ <?php echo number_format($row_user->earnamt, 2); ?></font><br>
                            	Total Earn
                            </div>
                        	-->
                            <div class="well icondashboard" align="center">
                            	<font color="green"> $ <?php echo number_format($row_user->deductamt, 2); ?></font><br>
                            	Total Balance
                            </div>
                                                        
                        	<div class="well icondashboard" align="center">
                            	<font color="blue"><?php echo $row_user->song_count; ?></font><br>
                            	Total Tracks	
                            </div>
                            
                        	<div class="well icondashboard" align="center">
                            	<font color="orange"><?php echo $row_user->love_count; ?></font><br>
                            	Total Love Count
                            </div>
                            
                        	<div class="well icondashboard" align="center">
                            	<font color="red"><?php echo $row_user->hate_count; ?></font><br>
                            	Total Hate Count
                            </div> 
                            
                        	<div class="well icondashboard" align="center">
                            	<font color="cyan"><?php echo $row_user->comment_count; ?></font><br>
                            	Total Comments
                            </div>                                                             
                          </div>
                       	</div>                      
                    <?php
						endif;
					?>
                   	<div class="panel panel-default" style="border: 1px solid #ccc;">
                    
                    	<div class='panel-body'>
                        
                        	<?php while($row = db_fetch_object($res)){  $song_details = $row; ?>
                            <div class="panel panel-default" style="border: 1px solid #ccc;">
                    			<div class='panel-body'>
                                    <div class="col-lg-3 ">
                                        <input type="hidden" id="lovehatestatus" />
                                        <a href="http://www.soundfan.com/<?php echo $song_details->song_url ;?> ">
                                        <?php 
                                            if($song_details->song_cover!='' && file_exists($song_details->song_cover))
                                                echo '<img class="thumbnail" src="'.$song_details->song_cover.'">';
                                            else
                                                echo '<img class="thumbnail" src="http://www.placehold.it/300x300/EFEFEF/AAAAAA&amp;text=Song+Cover+Photo" >';
                                        ?>
                                        </a>
                                    </div>	
                                    <div class="col-lg-9">
                                        <div class="col-lg-5" align="left">
                                            <br> <a href="http://www.soundfan.com/<?php echo $song_details->song_url ;?> "> Play this Song </a> 
                                            <br> Date Posted : 	<?php echo $song_details->dt; ?>
                                            <br> Uploaded By : 	<?php echo get_username($song_details->enteredby); ?>
                                        </div>
                                        <div class="col-lg-7">
                                        	<br /> Love Count: 	<?php echo $song_details->love; ?>
                                            <br /> Hate Count: 	<?php echo $song_details->hate; ?>
                                            <br />	<a href='https://twitter.com/share?url=http://www.soundfan.com/<?php echo $row->song_url; ?>' target='_blank'><b>Share Via Twitter </b></a> | 
                                            		<a href='https://www.facebook.com/sharer/sharer.php?u=http://www.soundfan.com/<?php echo $row->song_url; ?>' target='_blank'><b>Share Via Facebook </b></a>
                                            
                                        </div>
                                        
                                        <div class="clearfix"><br><br></div>
			                            <div class="col-lg-12">
                                    		<?php get_comments($row->song_id); ?>
	                                    </div>
                                        
                                        
                                 	</div>
                        		</div>
                      		</div>
	
                            <?php } ?>
                      	</div>
                    </div>
               </div>
        	</center>
      	</div>
        
        <?php
			}
			else
			{
		?>
         <div class="row">
        	<center>
                <div class="col-lg-1"></div>            
                <div class="col-lg-10">
                	<div class='page-header'>
						<h1 align="left">My Songs</h1>
                    </div> 
                    
                   	<div class="panel panel-default" style="border: 1px solid #ccc;">
                    	<div class='panel-body'>
        				<?php	echo "Sorry You Have Not Uploaded Any Songs";	?>
        				</div>
                    </div>
              </div>
          </center>
          </div>
        <?php } ?>
   	</div>

<?php
	include("template/footer.php");
?>